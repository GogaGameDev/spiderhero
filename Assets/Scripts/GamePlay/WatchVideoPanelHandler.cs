﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
public class WatchVideoPanelHandler : MonoBehaviour
{
    [Header("Header And Attributes")]
    public GameObject Header;
    public GameObject VideoText;
    public GameObject WatchVideoBtn;
    public GameObject NoThanksBtn;
    public GameObject FG;
    
    public void OpenWatchVideoPanel()
    {
        FG.transform.DOLocalMoveX(0.1f, 0.3f).OnComplete(delegate
        {
            VideoText.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
            {
                WatchVideoBtn.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
            {
                NoThanksBtn.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
                {
                    WatchVideoBtn.transform.DOScale(0.9f, 0.2f).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);
                    //TimesUp.transform.DOScale(1.1f, 0.5f).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);
                    Time.timeScale = 0;
                });
           });
        });
        });
        
    }

    public void CloseWatchVideoPanel()
    {
        NoThanksBtn.transform.DOScale(0f, 0.2f).SetEase(Ease.InBounce).SetUpdate(true).OnComplete(delegate
        {
            WatchVideoBtn.transform.DOScale(0f, 0.2f).SetEase(Ease.InBounce).SetUpdate(true).OnComplete(delegate
            {
                  
                    FG.transform.DOLocalMoveX(-990f, 0.3f);
                   
                        UImanager.instance.watchVideoPanel.SetActive(false);
                        UImanager.instance.LevelFailedPanel();
                    });
            });
           
    }
    public void CloseWatchVideoOnsuccesful()
    {
        NoThanksBtn.transform.DOScale(0f, 0.2f).SetEase(Ease.InBounce).SetUpdate(true).OnComplete(delegate
        {
            WatchVideoBtn.transform.DOScale(0f, 0.2f).SetEase(Ease.InBounce).SetUpdate(true).OnComplete(delegate
            {
                  
                        FG.transform.DOLocalMoveX(-990f, 0.3f);
                            
                            
                            UImanager.instance.watchVideoPanel.SetActive(false);
                        GamePlayHandler.instance.PlayerCanvas.enabled = true;


                    });
                });
    }
}
