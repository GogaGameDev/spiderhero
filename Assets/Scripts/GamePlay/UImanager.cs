﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using UnityEngine.Advertisements;
using NewHelicopter;
public class UImanager : MonoBehaviour
{
    public static UImanager instance;
    [Header("Btns")]
    public GameObject DropOffBtn;
    public GameObject PickPasBtn;
    //public GameObject PatrolBtn;
    public Image PatrolSlider;
    [Header("Engine Btn")]
    public GameObject EngineBtn;
    public Sprite EngineOn;
    public Sprite EngineOff;
    [Header("Panels")]
    public GameObject PausePanel;
    public GameObject LevelFailed;
    public GameObject CompleteLevel;
    public GameObject watchVideoPanel;
    public GameObject FadeImgPanel;
    public GameObject WayPointCanvas;
    [Header("Objective")]
    public GameObject ObjectiveImg;
    public Text Objective;
    [Header("Coins")]
    public Text coins;
    public GameObject CoinsPrefab;
    public Transform CoinsPos;
    public float time;
    public GameObject Alert;

    [Header("Map")]
    public Image Map;
    public Sprite MapOn;
    public Sprite MapOff;
    [Header("GameData")]
    public GameData GData;
    [HideInInspector]
    public UnityEvent OnLevelComplete;
    [HideInInspector]
    public UnityEvent OnLevelFailed;
    [HideInInspector]
    public UnityEvent WatchAdSuccessfully;
    [HideInInspector]
    public UnityEvent TimesUp;
    [Header("Cross Promo Link")]
    public string CrossPromoLinkAndroid;
    public string CrossPromoLinkIOS;
    [Header("Cross Promo Things")]
    public GameObject CrossPromoPanel;
    public GameObject LogoImg;
    public GameObject GameName;
    public GameObject PlayBTn;
    public GameObject AdBTn;
    public bool WatchAd = false;
    public GameObject TimerImg;
    [Header("Tutorials")]
    public GameObject Hand;
    public GameObject HandA;
    [Header("Help")]
    public GameObject PatrolIcon;
    public Button PatrolBtn;
    public bool AdforTime = false;
    public bool AdforPatrol = false;
    public Text NumberOfPass;
    public int TotalPass;
    public bool OutOfFuel = false;
    private void Awake()
    {
        instance = this;
    }
    public void Start()
    {
        if(GameManager.Instance.ModSelected == 0) { 
        TotalPass = GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers.Length;
        NumberOfPass.text = TotalPass.ToString();
        }
        Hand.transform.DOScale(1.2f, 0.3f).SetLoops(-1, LoopType.Yoyo);
        HandA.transform.DOScale(1.2f, 0.3f).SetLoops(-1, LoopType.Yoyo);
        coins.text = GData.Coins.ToString();
        OnLevelComplete.AddListener(delegate {
            StartCoroutine(DelayInLevelComplete());
            
        });
        OnLevelFailed.AddListener(delegate {
            StartCoroutine(DelayInLevelFailed());

        });
        WatchAdSuccessfully.AddListener(delegate {

            //Debug.Log("Add watch successfully");
            //MapPanel.GetComponent<MapHandler>().ShowHint();
                //PatrolBtn.interactable = false;
                //AdforPatrol = false;
                //Hand2.SetActive(false);
                //PatrolIcon.GetComponent<Animator>().enabled = false;
                GamePlayHandler.instance.Player[GameManager.Instance.BikeSelected].GetComponent<HelicopterController>().patrolRemaining += 40f;
                PatrolSlider.fillAmount = GamePlayHandler.instance.Player[GameManager.Instance.BikeSelected].GetComponent<HelicopterController>().patrolRemaining/100;
            Time.timeScale = 1;
            watchVideoPanel.GetComponent<WatchVideoPanelHandler>().CloseWatchVideoOnsuccesful();
            WayPointCanvas.GetComponent<Canvas>().enabled = true;
            OutOfFuel = true;
        });
        TimesUp.AddListener(delegate
        {
            OpenWatchVideoPanel();
            AdforTime = true;
        });
    }
    IEnumerator DelayInLevelComplete()
    {
        GamePlayHandler.instance.PlayerCanvas.enabled = false;
         yield return new WaitForSeconds(3);
        LevelComplete();
    }
    IEnumerator DelayInLevelFailed()
    {
        yield return new WaitForSeconds(3);
        LevelFailedPanel();
    }
    IEnumerator DelayinCrossPromo()
    {

        CrossPromoPanel.SetActive(true);
        yield return new WaitForSeconds(2);
        LogoImg.SetActive(true);
        GameName.SetActive(true);
        PlayBTn.SetActive(true);
        AdBTn.SetActive(true);
    }
    public void Coinsdetection()
    {
        StartCoroutine(GenerateCoins());
    }
    IEnumerator GenerateCoins()
    {
        for (int i = 0; i < 5; i++)
        {
            var Canvas = GameObject.Find("PlayerControlCanvas").transform;
            Debug.Log(i);
            yield return new WaitForSeconds(0.2f);
            SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.Chips);
            GData.Coins--;
            coins.text = GData.Coins.ToString();
            var go = Instantiate(CoinsPrefab, CoinsPos.position, Quaternion.identity, Canvas);
            //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.Chips);
            Destroy(go, time + 0.7f);
        }
    }
    public void ShowAlert()
    {
        StartCoroutine(AlertAnim());
    }
    IEnumerator AlertAnim()
    {
        Alert.SetActive(true);
        Alert.transform.DOScale(1.1f, 0.6f);
        yield return new WaitForSeconds(0.6f);
        Alert.transform.DOScale(1, 0.6f).OnComplete(delegate {

            Alert.SetActive(false);
        
        });
    }
    public void Pause()
    {
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        //GamePlayHandler.instance.PlayerCanvas.enabled = false;
        PausePanel.SetActive(true);
        PausePanel.transform.DOScale(1f, 0.1f).SetEase(Ease.OutBounce).OnComplete(delegate
        {
            PausePanel.GetComponent<PausePanelHandler>().OpenPausePanel();
        });
    }
    public void Resume()
    {
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        PausePanel.GetComponent<PausePanelHandler>().ClosePausePanel();
    }
    public void Next()
    {
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        if (GameManager.Instance.LevelSelected < 14)
        {
            GameManager.Instance.LevelSelected += 1;
        }
        else if (GameManager.Instance.LevelSelected > 14)
        {
            GameManager.Instance.LevelSelected = 14;
        }
        GameManager.Instance.ChangeScene("GamePlay");
    }
    public void Restart()
    {
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.CloseButtonClip);
        Time.timeScale = 1;
        GameManager.Instance.ChangeScene("GamePlay");
    }
    public void MainMenu()
    {
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        MonetizationManager.instance.ShowInterstitialMediation();
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.CloseButtonClip);
        Time.timeScale = 1;
        GameManager.Instance.ChangeScene("MainMenu");
    }
    public void LevelComplete()
    {
        Debug.Log("Level Complete//////////////");
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.LevelSuccessClip);
        GamePlayHandler.instance.PlayerCanvas.enabled = false;
        CompleteLevel.SetActive(true);
        GData.Coins += GamePlayHandler.instance.TodaysEarning;
        CompleteLevel.gameObject.transform.DOScale(1f, 0.25f).SetEase(Ease.OutBounce).OnComplete(delegate
        {
            CompleteLevel.GetComponent<CompleteLevelOpening>().OpenPannel();
            if (GameManager.Instance.LevelSelected >= PlayerPrefs.GetInt("Level"))
            {
                PlayerPrefs.SetInt("Level", GameManager.Instance.LevelSelected + 1);
                Debug.Log("Level Unlock " + PlayerPrefs.GetInt("Level"));
            }
            //if (!GData.RemoveAds)
            //{

            //        Debug.Log("Showing cross promo Ad");
            //        StartCoroutine(DelayinCrossPromo());


            //}
        });
        //GData.PatrolLeft = GamePlayHandler.instance.Player[GameManager.Instance.BikeSelected].GetComponent<HelicopterController>().patrolRemaining;

    }
    public void LevelFailedPanel()
    {
        Debug.Log("Level Failed//////////////");
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.LevelSuccessClip);
        //GamePlayHandler.instance.PlayerCanvas.enabled = false;
        WayPointCanvas.GetComponent<Canvas>().enabled = false;
        LevelFailed.SetActive(true);
        LevelFailed.gameObject.transform.DOScale(1f, 0.25f).SetUpdate(true).OnComplete(delegate
        {
            LevelFailed.GetComponent<LevelFailedHandler>().OpenPannel();
            //if (!GData.RemoveAds)
            //{
            //        Debug.Log("Showing cross promo Ad");
            //        StartCoroutine(DelayinCrossPromo());

            //}
        });
    }
    public void OnCrossPromoBtnClick()
    {
#if UNITY_ANDROID
        Application.OpenURL(CrossPromoLinkAndroid);
#elif UNITY_IOS
        Application.OpenURL(CrossPromoLinkIOS);

#endif
    }
    public void WatchVideoforHint()
    {
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        MonetizationManager.instance.ShowRewardedVideoMediation();
    }
    public void OnHintBtnClick()
    {
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        //watchVideoPanel.SetActive(true);
        //watchVideoPanel.GetComponent<WatchVideoPanelHandler>().OpenWatchVideoPanel();
    }
    public void OpenWatchVideoPanel()
    {
        GamePlayHandler.instance.PlayerCanvas.enabled = false;
        watchVideoPanel.SetActive(true);
        WayPointCanvas.GetComponent<Canvas>().enabled = false;
        watchVideoPanel.GetComponent<WatchVideoPanelHandler>().OpenWatchVideoPanel();
    }
    public void NothanksBtn()
    {
        watchVideoPanel.GetComponent<WatchVideoPanelHandler>().CloseWatchVideoPanel();

    }
}
