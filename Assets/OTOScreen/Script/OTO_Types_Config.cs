﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
namespace OTO

{   [System.Serializable]
	public class OTO_Types_Config
	{
		[JsonProperty("id")]
		public string id;
		[JsonProperty("all-oto-iap-remove-ads")]
		public bool all_oto_iap_remove_ads;
		[JsonProperty("ImagesConfig")]
		public ImagesConfig ImagesConfig;
		[JsonProperty("FullPriceConfig")]
		public FullPriceConfig FullPriceConfig;
		[JsonProperty("DiscountPriceConfig")]
		public DiscountPriceConfig DiscountPriceConfig;
		[JsonProperty("OrderBumpPriceConfig")]
		public OrderBumpPriceConfig OrderBumpPriceConfig;
		[JsonProperty("Button-Text")]
		public Button_Text Button_Text;
		[JsonProperty("DiscountPercent")]
		public int DiscountPercent;
		[JsonProperty("AreYouSureQonversionID")]
		public string AreYouSureQonversionID;

	}

	[System.Serializable]
	public class ImagesConfig
	{
		[JsonProperty("header-title-image")]
		public string header_title_image;
		[JsonProperty("copy-image")]
		public string copy_image;
		[JsonProperty("offer-disappears-image")]
		public string offer_disappears_image;
		[JsonProperty("no-thanks-image")]
		public string no_thanks_image;

	}
	[System.Serializable]
	public class FullPriceConfig
	{
		[JsonProperty("QonversionID")]
		public string QonversionID;
		[JsonProperty("Pre-Price-Text")]
		public Pre_Price_Text Pre_Price_Text;
		[JsonProperty("Price")]
		public float Price;
		[JsonProperty("Post-Price-Text")]
		public Post_Price_Text Post_Price_Text;

	}
	
	[System.Serializable]
	public class DiscountPriceConfig
	{
		[JsonProperty("QonversionID")]
		public string QonversionID;
		[JsonProperty("Pre-Price-Text")]
		public Pre_Price_Text Pre_Price_Text;
		[JsonProperty("Price")]
		public float Price;
		[JsonProperty("Post-Price-Text")]
		public Post_Price_Text Post_Price_Text;
	}

	[System.Serializable]
	public class OrderBumpPriceConfig
	{
		[JsonProperty("QonversionID")]
		public string QonversionID;
		[JsonProperty("Pre-Price-Text")]
		public Pre_Price_Text Pre_Price_Text;
		[JsonProperty("Price")]
		public float Price;
		[JsonProperty("Post-Price-Text")]
		public Post_Price_Text Post_Price_Text;

	}

	[System.Serializable]
	public class Pre_Price_Text
	{
		[JsonProperty("en")]
		public string en;
		[JsonProperty("de")]
		public string de;
		[JsonProperty("pt")]
		public string pt;
		[JsonProperty("ru")]
		public string ru;
		[JsonProperty("es")]
		public string es;
		[JsonProperty("it")]
		public string it;
		[JsonProperty("tr")]
		public string tr;
		[JsonProperty("fr")]
		public string fr;
		[JsonProperty("zh")]
		public string zh;
		[JsonProperty("jp")]
		public string jp;
		[JsonProperty("kr")]
		public string kr;
		[JsonProperty("ar")]
		public string ar;
	}
	[System.Serializable]
	public class Post_Price_Text
	{
		[JsonProperty("en")]
		public string en;
		[JsonProperty("de")]
		public string de;
		[JsonProperty("pt")]
		public string pt;
		[JsonProperty("ru")]
		public string ru;
		[JsonProperty("es")]
		public string es;
		[JsonProperty("it")]
		public string it;
		[JsonProperty("tr")]
		public string tr;
		[JsonProperty("fr")]
		public string fr;
		[JsonProperty("zh")]
		public string zh;
		[JsonProperty("jp")]
		public string jp;
		[JsonProperty("kr")]
		public string kr;
		[JsonProperty("ar")]
		public string ar;
	}
	[System.Serializable]
	public class Button_Text
	{
		[JsonProperty("en")]
		public string en;
		[JsonProperty("de")]
		public string de;
		[JsonProperty("pt")]
		public string pt;
		[JsonProperty("ru")]
		public string ru;
		[JsonProperty("es")]
		public string es;
		[JsonProperty("it")]
		public string it;
		[JsonProperty("tr")]
		public string tr;
		[JsonProperty("fr")]
		public string fr;
		[JsonProperty("zh")]
		public string zh;
		[JsonProperty("jp")]
		public string jp;
		[JsonProperty("kr")]
		public string kr;
		[JsonProperty("ar")]
		public string ar;
	}
}