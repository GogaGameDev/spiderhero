﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text;
using UnityEngine.SceneManagement;
using System;
using System.Linq;
using OTO;

public class OTOScreenVisual : MonoBehaviour
{
    // OTOManager manager;
    OTOManager_RemoteConfig manager;
    IAP iapManager;
    public RectTransform middleRect;

    public GameObject ScreenObject;
    public Image image;
    public Image checkMarkImage;
    public Sprite[] checkMarkSprites;
    public bool isChecked;
    public Text text1;
    public Text text2;
    public Text buttonText;
    bool isOnIpad;
    ScreenOrientation originalScreenOrientation;

    [SerializeField] ShopProductNames _checked;
    [SerializeField] string _checked_String;
    [SerializeField] ShopProductNames _uncheacked;
    [SerializeField] string _uncheacked_String;
    [SerializeField] ShopProductNames _regularID;
    [SerializeField] string _regularID_String;



    private void Awake()
    {
        originalScreenOrientation = Screen.orientation;
    }
    private void Start()
    {
        image.preserveAspect = true;
       
        if (originalScreenOrientation != ScreenOrientation.Portrait)
        {
            Screen.orientation = ScreenOrientation.Portrait;
            Debug.Log("Screen changed to " + Screen.orientation);
        }
        iapManager = GetComponent<IAP>();
        iapManager.OnPurchase.AddListener(Close);
        //iapManager.OnInitFail.AddListener(NextScene);
        iapManager.OnInitSucess.AddListener(ShowObject);
#if UNITY_EDITOR||UNITY_IOS
        isOnIpad = UnityEngine.iOS.Device.generation.ToString().Contains("iPad");
#endif
        // manager = GetComponent<OTOManager>();
        manager = GetComponent<OTOManager_RemoteConfig>();
        manager.OnDataLoaded.AddListener(SetText);
        manager.OnDataLoadFailed.AddListener(NextScene);
    }

    public void ToggleCheckMark()
    {
        isChecked = !isChecked;
        checkMarkImage.sprite = isChecked ? checkMarkSprites[0] : checkMarkSprites[1];
    }
    public void Buy()
    {
        if (app_ads_flag.qonversion_enabled)
        {
            string id = isChecked ? _checked_String : _uncheacked_String;

            iapManager.BuyIAP(id, isChecked);
        }
        else
        {
            ShopProductNames id = isChecked ? _checked : _uncheacked;
            iapManager.BuyIAP(id, isChecked);


        }


    }


    private void Update()
    {
        LayoutRebuilder.ForceRebuildLayoutImmediate(middleRect);
    }
    public void SetText()
    {

        OTOScreen screen = manager.otoScreen;
        iapManager.Init();
        SetInaps(screen);
        text1.text = isOnIpad ? screen.regular_pricePrefix_ipad : screen.regular_price_prefix;
        text1.text += " <color=red>$" + screen.regular_price + "</color>" + screen.regular_price_text_prefix;
        text1.text += "\n" + (isOnIpad ? screen.discount_price_text_prefix_ipad : screen.discount_price_text_prefix);
        text1.text += " <color=green>$" + screen.discounted_price + "</color>" + screen.ob_price_text_prefix;

      
 

        string text = isOnIpad ? screen.discount_plus_ob_prefix_ipad : screen.discount_plus_ob_prefix;
        int id = text.IndexOf("for");
        string pretext = text.Substring(0, id);
        string afterText = text.Substring(id, text.Length - id);
        Debug.Log("pretext " + pretext);
        Debug.Log("afterText " + afterText);
        text2.text = pretext + "\n" + afterText;
        text2.text += "<color=green> $" + screen.discounted_plus_ob_price + "" + screen.ob_price_text_prefix + "</color>";
        buttonText.text = screen.unlock_button_prefix + " (" + screen.discount_percent + "% off)";


      
    }
    void SetInaps(OTOScreen screen) {

        List<ShopProductNames> productList = Enum.GetValues(typeof(ShopProductNames)).Cast<ShopProductNames>().ToList();
         _checked_String = screen.checked_iap_product_id;
         _uncheacked_String = screen.unchecked_iap_product_id;
         _regularID_String = screen.regular_iap_product_id;

      string  checkedID  = _checked_String.Replace(".", "");
       string unChecked = _uncheacked_String.Replace(".", "");
      string  regular = _regularID_String.Replace(".", "");
        iapManager.regularID = _regularID_String;

        _checked = productList.FirstOrDefault(x => x.ToString() == checkedID);
        _uncheacked = productList.FirstOrDefault(x => x.ToString() == unChecked);
        _regularID = productList.FirstOrDefault(x => x.ToString() == regular);


        iapManager.regularID = screen.regular_iap_product_id;
        iapManager.regular = _regularID;

    }
    void ShowObject()
    {
        if (manager._oto_settings.hide_oto)
        {
            return;
        }
        Debug.Log("ShowObject");

        bool show = false;

        if (manager._oto_settings.show_oto_on_app_starts)
        {
            show = true;
        }
        else
        {
            int showed = PlayerPrefs.GetInt("Showed", 0);
            if (showed == 0)
            { // checking for one time only
                show = true;
            }
        }
        Debug.Log("Show"+ show);
        if (show)
        {

            ScreenObject.gameObject.SetActive(true);
            FireBaseAnalitycs.instance.OnOTOShownSend(manager._oto_settings.offer_type);
        }
        else
        {
            NextScene();
        }
    }
    public void Close()
    {

        PlayerPrefs.SetInt("Showed", 1);
        ScreenObject.gameObject.SetActive(false);
        NextScene();
    }
    public void  Trigger_SkipOTO() {
        FireBaseAnalitycs.instance.OnSkipOTOEvent_Send(GameProps.ottoOffer);
        Close();
    }
    public void NextScene()
    {
        if (originalScreenOrientation != ScreenOrientation.Portrait)
        {
            StartCoroutine(nextSceneRoutine());
        }
        else {
            SceneManager.LoadScene(1);
        }
    }
    IEnumerator nextSceneRoutine()
    {
        Screen.orientation = originalScreenOrientation;
        yield return new WaitForSeconds(0.5f);
        SceneManager.LoadScene(1);

    }
}
