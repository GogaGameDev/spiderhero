﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class RickshawHandler : MonoBehaviour
{

    public GameObject[] Bikes;
    public GameObject[] BikesSpecs;
    public Text Coins;
    public GameData Gdata;
    public GameObject UnlockBtns2;
    public GameObject UnlockBtns3;
    public GameObject SelectBtn;
    public GameObject Alert;
    
    // Start is called before the first frame update
    void Start()
    {
        
        Coins.text = Gdata.Coins.ToString();
        for (int i = 0; i < Bikes.Length; i++)
        {
            if(i == GameManager.Instance.BikeSelected)
            {
                Bikes[i].SetActive(true);
                BikesSpecs[i].SetActive(true);
            }
            else
            {
                BikesSpecs[i].SetActive(false);
                Bikes[i].SetActive(false);
            }
        }
        MonetizationManager.instance.HideBannerAd();
    }
    public void RightBtnClick()
    {
        if(GameManager.Instance.BikeSelected < 2)
        {

            GameManager.Instance.BikeSelected++;
            for (int i = 0; i < Bikes.Length; i++)
            {
                Debug.Log(i);
                if (i == GameManager.Instance.BikeSelected)
                {
                    Bikes[i].SetActive(true);
                    BikesSpecs[i].SetActive(true);
                    if (i == 1 && GameManager.Instance.Gdata.UnLockedRickshaw2)
                    {
                        Debug.Log("Condition 1");
                        UnlockBtns2.SetActive(false);
                        UnlockBtns3.SetActive(false);
                        SelectBtn.SetActive(true);
                    }
                    else if (i == 2 && GameManager.Instance.Gdata.UnLockedRickshaw3)
                    {
                        Debug.Log("Condition 2");
                        UnlockBtns3.SetActive(false);
                        SelectBtn.SetActive(true);
                        UnlockBtns2.SetActive(false);

                    }
                    else if (i == 1 && !GameManager.Instance.Gdata.UnLockedRickshaw2)
                    {
                        Debug.Log("Condition 3");
                        UnlockBtns2.SetActive(true);
                        UnlockBtns3.SetActive(false);
                        SelectBtn.SetActive(false);
                    }
                    else if (i == 2 && !GameManager.Instance.Gdata.UnLockedRickshaw3)
                    {
                        Debug.Log("Condition 4");
                        UnlockBtns2.SetActive(false);
                        UnlockBtns3.SetActive(true);
                        SelectBtn.SetActive(false);
                    }
                }
                
                else
                {
                    Bikes[i].SetActive(false);
                BikesSpecs[i].SetActive(false);

                }



            }
            
        }
        else
        {
            GameManager.Instance.BikeSelected = 2;
        }
    }
    public void LeftBtnClick()
    {
        if (GameManager.Instance.BikeSelected > 0)
        {
            GameManager.Instance.BikeSelected--;
            for (int i = 0; i < Bikes.Length; i++)
            {
                if (i == GameManager.Instance.BikeSelected)
                {
                    Bikes[i].SetActive(true);
                    BikesSpecs[i].SetActive(true);
                    if (i == 1 && GameManager.Instance.Gdata.UnLockedRickshaw2)
                    {
                        UnlockBtns2.SetActive(false);
                        UnlockBtns3.SetActive(false);
                        SelectBtn.SetActive(true);
                    }
                    else if (i == 2 && GameManager.Instance.Gdata.UnLockedRickshaw3)
                    {
                        UnlockBtns3.SetActive(false);
                        SelectBtn.SetActive(true);
                        UnlockBtns2.SetActive(false);

                    }
                    else if (i == 1 && !GameManager.Instance.Gdata.UnLockedRickshaw2)
                    {
                        UnlockBtns2.SetActive(true);
                        UnlockBtns3.SetActive(false);
                        SelectBtn.SetActive(false);
                    }
                    else if (i == 2 && !GameManager.Instance.Gdata.UnLockedRickshaw3)
                    {
                        UnlockBtns2.SetActive(false);
                        UnlockBtns3.SetActive(true);
                        SelectBtn.SetActive(false);
                    }
                    else
                    {
                        UnlockBtns2.SetActive(false);
                        UnlockBtns3.SetActive(false);
                        SelectBtn.SetActive(true);
                    }
                }
                else
                {
                    Bikes[i].SetActive(false);
                    BikesSpecs[i].SetActive(false);
                }

            }
        }
        else
        {
            GameManager.Instance.BikeSelected = 0;
        }
    }
    public void Next()
    {
        GameManager.Instance.ChangeScene("Loading");
        MonetizationManager.instance.ShowInterstitialMediation();
    }
    public void Back()
    {
        switch (GameManager.Instance.ModSelected)
        {
            case 0:
                GameManager.Instance.ChangeScene("LevelSelection");
                break;
            case 1:
                GameManager.Instance.ChangeScene("MainMenu");
                break;

        }
    }
    public void Unlock(int price)
    {
        if(GameManager.Instance.Gdata.Coins>= price)
        {
            GameManager.Instance.Gdata.Coins -= price;
            if(GameManager.Instance.BikeSelected == 1)
            {
                GameManager.Instance.Gdata.UnLockedRickshaw2 = true;
                UnlockBtns2.SetActive(false);
                UnlockBtns3.SetActive(false);
                SelectBtn.SetActive(true);
            }
            else if(GameManager.Instance.BikeSelected == 2)
            {
                
                    GameManager.Instance.Gdata.UnLockedRickshaw3 = true;
                    UnlockBtns2.SetActive(false);
                    UnlockBtns3.SetActive(false);
                    SelectBtn.SetActive(true);
                
            }
            Coins.text = GameManager.Instance.Gdata.Coins.ToString();
            if(PlayFabManager.Instance!=null)
            PlayFabManager.Instance.SendGameData();
        }
        else
        {
            Alert.SetActive(true);
            Alert.transform.DOScale(1.1f, 0.4f).OnComplete(delegate
            {
                Alert.transform.DOScale(1f, 0.4f).OnComplete(delegate
                {
                    Alert.SetActive(false);

                });

            });
        }
    }
}
