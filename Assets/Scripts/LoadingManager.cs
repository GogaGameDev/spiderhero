﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;
public class LoadingManager : MonoBehaviour
{
    public Image LoadingBar;
    public GameObject Rotator;
    public Text LoadingProgress;
    //public GameObject Piggy;

    // Start is called before the first frame update
    void Start()
    {
        //Piggy.transform.DORotate(new Vector3(0, 0, -20f), 0.5f).SetEase(Ease.OutBounce).SetLoops(-1,LoopType.Yoyo);
        
        StartCoroutine(LoadLevel());
    }

    IEnumerator LoadLevel()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync("GamePlay");
        while (!operation.isDone)
        {
            float progress = Mathf.Clamp01(operation.progress / 0.9f);
            LoadingBar.fillAmount = progress;
            //Rotator.transform.Rotate(new Vector3(0, 0, progress * Time.deltaTime));
            //LoadingProgress.text = (progress * 100f).ToString("00") + "%";
            //Debug.Log(operation.progress);

            yield return null;
        }
    }
    
}
