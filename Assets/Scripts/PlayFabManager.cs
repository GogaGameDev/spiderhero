﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayFab;
using PlayFab.ClientModels;
using Newtonsoft.Json;
using System;

public class PlayFabManager : MonoBehaviour {

    [HideInInspector]
    public string UserName="TESTUSER";
    [HideInInspector]
    public string UserPassword="TESTPASSWORD";

    [Header("Game Title ID PlayFab")]
    public string TitleID = "8E30";

    [Header("PlatForm Check PC")]
    public bool PC=false;

    [Header("PlatForm Check Andriod")]
    public bool Andriod = false;

    [Header("PlatForm Check iOS")]
    public bool iOS = false;

    [Header("Login Check")]
    public bool IsLogin = false;

    string DeviceId = "";
    string DeviceName = "";
    string OS="";
    private string _playerName = "";
    private string _playFabUserIdentifier = null;

    private bool _isLeaderboardReady = false;

    LoginWithCustomIDRequest customIDRequest = new LoginWithCustomIDRequest();
    LoginWithAndroidDeviceIDRequest AndriodDeviceRequest = new LoginWithAndroidDeviceIDRequest();
    LoginWithIOSDeviceIDRequest iOSDeviceRequest = new LoginWithIOSDeviceIDRequest();
    UpdateUserTitleDisplayNameRequest UpdateTitleNameRequest = new UpdateUserTitleDisplayNameRequest();

    public static PlayFabManager Instance = null;

    [Header("Scriptable Objects")]

    public GameData Gdata;
    //public IAPdata iapdata;
    //public InAppPurchasesData IAPData;

    public enum PlayFabStats
    {
        GameData,
        InAppPurchasesData,
        TotalScore
    }

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }

    void Start()
    {
        GetSavedSettings();
        if (PC)
        {
            CustomIdRquestParameterSettings();
            PlayFabClientAPI.LoginWithCustomID(customIDRequest, CustomIdLoginResultCallBack, CustomIdLoginErrorCallBack, null);
        }

        if (Andriod)
        {
            GetDeviceID_Name();
            AndriodIdRquestParameterSettings();

            PlayFabClientAPI.LoginWithAndroidDeviceID(AndriodDeviceRequest, 
                (result) => {

                    result.ToString();

                    if (result.NewlyCreated)
                    {
                        Debug.Log("User Created First Time");
                        _playFabUserIdentifier = result.PlayFabId;

                        if (PlayerPrefs.GetInt("SetPlayerName") != 1)
                        {
                            Debug.Log("Poping Out Naming Pannel");
                            //MainMenuHandler.instance.PopOutNamingPannel();
                        }
                        else
                        {
                            UpdatePlayerDisplayName();
                            UpdatePlayerStat(PlayFabStats.TotalScore.ToString(), Gdata.TotalScores);

                        }
                        SendGameData();
                        IsLogin = true;
                    }

                    else
                    {
                        Debug.Log("User was Already Created");
                        GetUserCustomData(PlayFabStats.GameData.ToString());
                        GetUserCustomData(PlayFabStats.InAppPurchasesData.ToString());

                        if (PlayerPrefs.GetInt("SetPlayerName") != 1)
                        {
                            Debug.Log("Poping Out Naming Pannel");
                            //MainMenuHandler.instance.PopOutNamingPannel();
                        }
                        else
                        {
                            UpdatePlayerDisplayName();
                            UpdatePlayerStat(PlayFabStats.TotalScore.ToString(), Gdata.TotalScores);

                        }



                        IsLogin = true;
                        UpdatePlayerStat(PlayFabStats.TotalScore.ToString(), Gdata.TotalScores);

                    }


                    if (IsLogin)
                    {
                        GetLeaderboard(PlayFabStats.TotalScore.ToString());
                    }

                }, 
                (error) => {
                    Debug.Log("Andriod Login Failed");
                    Debug.Log("Error is " + error.ErrorMessage);
                    IsLogin = false;
                });
        }

        if (iOS)
        {
            GetDeviceID_Name();
            iOSIdRquestParameterSettings();

            Social.localUser.Authenticate((success) =>
            {
                if (success || Social.localUser.authenticated)
                    LoginWithiOS(Social.localUser.id);
                else
                    Debug.Log("Login failed");
            });


        }



    }

    void LoginWithiOS(string id)
    {
        var request = new LoginWithGameCenterRequest()
        {
            TitleId = TitleID,
            CreateAccount = true,
            PlayerId = id
        };

        PlayFabClientAPI.LoginWithGameCenter(request, (result) =>
        {

            if (result.NewlyCreated)
            {
                Debug.Log("User Created First Time");
                _playFabUserIdentifier = result.PlayFabId;

                if (PlayerPrefs.GetInt("SetPlayerName") != 1)
                {
                    Debug.Log("Poping Out Naming Pannel");
                    //MainMenuHandler.instance.PopOutNamingPannel();
                }
                else
                {
                    UpdatePlayerDisplayName();
                    UpdatePlayerStat(PlayFabStats.TotalScore.ToString(), Gdata.TotalScores);

                }
                SendGameData();
                IsLogin = true;
            }

            else
            {
                Debug.Log("User was Already Created");
                GetUserCustomData(PlayFabStats.GameData.ToString());
                GetUserCustomData(PlayFabStats.InAppPurchasesData.ToString());

                if (PlayerPrefs.GetInt("SetPlayerName") != 1)
                {
                    Debug.Log("Poping Out Naming Pannel");
                    //MainMenuHandler.instance.PopOutNamingPannel();
                }
                else
                {
                    UpdatePlayerDisplayName();
                    UpdatePlayerStat(PlayFabStats.TotalScore.ToString(), Gdata.TotalScores);

                }



                IsLogin = true;
                UpdatePlayerStat(PlayFabStats.TotalScore.ToString(), Gdata.TotalScores);

            }


            if (IsLogin)
            {
                GetLeaderboard(PlayFabStats.TotalScore.ToString());
            }

        },
                (error) => {
                    Debug.Log("Andriod Login Failed");
                    Debug.Log("Error is " + error.ErrorMessage);
                    IsLogin = false;
                });
    
}


    public void GetSavedSettings()
    {
        if (PlayerPrefs.HasKey(PlayFabStats.GameData.ToString()))
        {
            UpdateDataLocally(PlayerPrefs.GetString(PlayFabStats.GameData.ToString()), "GameData");
        }

        if (PlayerPrefs.HasKey(PlayFabStats.InAppPurchasesData.ToString()))
        {
            UpdateDataLocally(PlayerPrefs.GetString(PlayFabStats.InAppPurchasesData.ToString()), "InAppPurchasesData");
        }

    }

    private void  GetDeviceID_Name()
    {
        DeviceId = SystemInfo.deviceUniqueIdentifier;
        DeviceName = SystemInfo.deviceName;
        OS = SystemInfo.operatingSystem;

    }         

    private void SetPlayerDisplayName()
    {
        //code for setting playerName
        Debug.Log("Setting player display name");
        UpdateTitleNameRequest.DisplayName = Gdata.PlayerName;
        SendGameData();
    }

    private void CustomIdRquestParameterSettings()
    {
        customIDRequest.CustomId = "StarterTesting";
        customIDRequest.CreateAccount = true;
        customIDRequest.TitleId = TitleID;
    }

    private void AndriodIdRquestParameterSettings()
    {
        
        AndriodDeviceRequest.AndroidDeviceId = DeviceId;
        AndriodDeviceRequest.AndroidDevice = DeviceName;
        AndriodDeviceRequest.OS = OS;
        AndriodDeviceRequest.CreateAccount = true;
        AndriodDeviceRequest.TitleId = TitleID;
    }

    private void iOSIdRquestParameterSettings()
    {

        iOSDeviceRequest.DeviceId = DeviceId;
        iOSDeviceRequest.DeviceModel = DeviceName;
        iOSDeviceRequest.CreateAccount = true;
        iOSDeviceRequest.TitleId = TitleID;
        iOSDeviceRequest.OS = OS;
    }

    public void UpdatePlayerDisplayName()
    {
        SetPlayerDisplayName();
        PlayFabClientAPI.UpdateUserTitleDisplayName(UpdateTitleNameRequest, UpdatePlayerDisplayNameResulCallBack, UpdatePlayerNameErrorCallBack, null);
        
    }

    public void SendGameData()
    {
        try
        {
            var st = JsonConvert.SerializeObject(Gdata);
            PlayerPrefs.SetString(PlayFabStats.GameData.ToString(), st);
            Debug.Log(st);
            SaveUserCustomDatatoServer(PlayFabStats.GameData.ToString(), st);


            //var cp = JsonConvert.SerializeObject(iapdata);
            //PlayerPrefs.SetString(PlayFabStats.InAppPurchasesData.ToString(), cp);
            //Debug.Log(cp);
            //SaveUserCustomDatatoServer(PlayFabStats.InAppPurchasesData.ToString(), cp);




        }
        catch (System.Exception ex)
        {
            Debug.Log(",.,.,..,.,.,., Exception././././//" + ex.Message);
        }
    }

    public void SaveUserCustomDatatoServer(string key, string value)
    {
        
            var request = new UpdateUserDataRequest()
            {
                // set Custome data here
                Data = new Dictionary<string, string>()
                {
                    {key, value}
                }
            };

            PlayFabClientAPI.UpdateUserData(request, (result) => { SendUserDatatoServer();  },
                (error) => { FailedPushingDatatoServer(); });
        
    }

    public void GetUserCustomData(string key)
    {
        var request = new GetUserDataRequest()
        {
            PlayFabId = _playFabUserIdentifier,
            Keys = null
        };

        PlayFabClientAPI.GetUserData(request, (result) =>
        {
            if ((result.Data == null) || (result.Data.Count == 0))
            {
                Debug.Log(result.Request);
                NullDataFetching();
            }
            else
            {
                foreach (var item in result.Data)
                {
                    if (item.Key == key)
                    {
                        var value = item.Value.Value;
                        UpdateDataLocally(value, key);                                   
                        break;
                    }
                }
            }
        }, (error) => { ErrorDataFetching(); });
    }

    private void UpdateDataLocally(string json, string key)
    {
        Debug.Log("Updating Data From Server");


        switch (key)
        {

            case "GameData":
                try
                {
                    var dg = JsonConvert.DeserializeObject<GameData>(json);


                    if (dg.LevelCompleted > Gdata.LevelCompleted)
                    {
                        Gdata.LevelCompleted = dg.LevelCompleted;
                    }
                    if (dg.Coins > Gdata.Coins)
                    {
                        //Gdata.Coins = dg.Coins;
                    }
                    //if (dg.PatrolLeft > Gdata.PatrolLeft)
                    //{
                    //    Gdata.PatrolLeft = dg.PatrolLeft;
                    //}
                    //if (dg.Rakes > Gdata.Rakes)
                    //{
                    //    Gdata.Rakes = dg.Rakes;
                    //}
                    //if (dg.Bombs > Gdata.Bombs)
                    //{

                    //    Gdata.Bombs = dg.Bombs;
                    //}
                    //if (dg.TotalScores > Gdata.TotalScores)
                    //{
                    //    Gdata.TotalScores = dg.TotalScores;
                    //}

                    //if (dg.Coins > Gdata.Coins)
                    //{
                    //    Gdata.Coins = dg.Coins;
                    //}

                    //if (dg.Cash > Gdata.Cash)
                    //{
                    //    Gdata.Cash = dg.Cash;
                    //}


                    if (dg.SetPlayerName == true)
                    {
                        Gdata.SetPlayerName = true;
                       // ChangeNameHandler.Instance.CloseNamePannel();
                    }

                    if (dg.PlayerName != null)
                    {
                        Gdata.PlayerName = dg.PlayerName;
                        //MainMenuHandler.Instance.UpdatePlayerName(Gdata.PlayerName);
                    }
                    if(dg.RemoveAds)
                    {
                        Gdata.RemoveAds = true;
                    }
                }


                catch (Exception e)
                {

                }

                break;

            case "InAppPurchasesData":
                try
                {
                   // var ss = JsonConvert.DeserializeObject<IAPdata>(json);

                    

                    //if (ss.IsConsumable3Purchased)
                    //{
                    //    IAPData.IsConsumable3Purchased = true;
                    //}

                    //if (ss.IsNonConsumable1Purchased)
                    //{
                    //    IAPData.IsNonConsumable1Purchased = true;
                    //}

                    //if (ss.IsNonConsumable2Purchased)
                    //{
                    //    IAPData.IsNonConsumable2Purchased = true;
                    //}

                    //if (ss.IsNonConsumable3Purchased)
                    //{
                    //    IAPData.IsNonConsumable3Purchased = true;
                    //}

                }


                catch (Exception e)
                {

                }

                break;






        }

        //SelectionManager.Instance.UpdateDataintoGame();
        //MainMenuHandler.instance.UpdateCoins();
        SendGameData();


    }

    public void UpdatePlayerStat(string nameOfStat, int value)
    {
        if (IsLogin)
        {
            //if (PlayerPrefs.GetInt("SetPlayerName") == 1)
            //{

                var stats = new StatisticUpdate { Value = value, StatisticName = nameOfStat, Version = uint.MinValue };
                var listofstats = new List<StatisticUpdate> { stats };
                var request = new UpdatePlayerStatisticsRequest
                {
                    Statistics = listofstats
                };

                PlayFabClientAPI.UpdatePlayerStatistics(request,
                    (result) => { OnUpdateStatSucessEvent(); },
                    (error) => { Debug.Log("Sending Stat Failed" + error.GenerateErrorReport()); });


            //}
        }
    }


    public void GetLeaderboard(string stat)
    {
        Debug.Log("HELOOOOOOO WORLD");
        if (IsLogin)
        {
            _isLeaderboardReady = false;
            var playersdata = new List<PlayerData>();
            var request = new GetLeaderboardRequest
            {
                MaxResultsCount = 100,
                StatisticName = stat,
                StartPosition = 0,
                ProfileConstraints = new PlayerProfileViewConstraints() { ShowOrigination = true, ShowDisplayName = true }

            };

            PlayFabClientAPI.GetLeaderboard(request, (result) =>
            {
                if ((result.Leaderboard == null))
                {
                    Debug.Log("Leader Board is Empty");
                }
                else
                {
                    foreach (var player in result.Leaderboard)
                    {
                        var data = new PlayerData();
                        data.DisplayName = player.DisplayName;
                        data.Position = player.Position;
                        data.Score = player.StatValue;
                        data.PlayerFabId = player.PlayFabId;

                        data.origination = player.Profile.Origination.Value;
                        playersdata.Add(data);

                    }
                    //LeaderboardManager.Instance.OnLeaderboardReadyEvent(stat, playersdata);
                    _isLeaderboardReady = true;
                }
            }, (error) => {
                Debug.Log("LeaderBoard Failed");
                Debug.Log(error);
            });
        }
    }

    //*********************************************************************************CALL BACKS***********************************************************************//
    void CustomIdLoginResultCallBack(LoginResult result)
    {
        if (result.NewlyCreated == true)
        {
            Debug.Log("User Created First Time");
            _playFabUserIdentifier = result.PlayFabId;

            //PopOut Naming Pannel for the first Time to set player Name
            if (PlayerPrefs.GetInt("SetPlayerName") != 1)
            {
                Debug.Log("HELOOOOOOOOOOOOOOO");
                //MainMenuHandler.instance.PopOutNamingPannel();
            }
            else
            {
                UpdatePlayerDisplayName();
                //UpdatePlayerStat(PlayFabStats.TotalScore.ToString(), Gdata.TotalScores);

            }

            SendGameData();
            IsLogin = true;
            UpdatePlayerStat(PlayFabStats.TotalScore.ToString(), Gdata.TotalScores);

          

        }

        else
        {
            Debug.Log("User was Already Created");
            GetUserCustomData(PlayFabStats.GameData.ToString());
            GetUserCustomData(PlayFabStats.InAppPurchasesData.ToString());

            IsLogin = true;
            UpdatePlayerStat(PlayFabStats.TotalScore.ToString(), Gdata.TotalScores);

        }


        if (IsLogin)
        {
            GetLeaderboard(PlayFabStats.TotalScore.ToString());
        }
    }

    void CustomIdLoginErrorCallBack(PlayFabError E)
    {
        Debug.Log("Custom Login Error"+ E.Error);
        IsLogin = false;
    }

    void UpdatePlayerNameErrorCallBack(PlayFabError E)
    {
        Debug.Log("Update Player Name Error" + E.Error);
    }

    void UpdatePlayerDisplayNameResulCallBack(UpdateUserTitleDisplayNameResult result)
    {
      Debug.Log("DisPlay Name Updated Sucessfully"+  result.ToString());
    }
    
    void SendUserDatatoServer()
    {
        Debug.Log("Data Send to PlayFab Server Sucessfully");
    }

    void FailedPushingDatatoServer()
    {
        Debug.Log("Data Send to PlayFab Server Sucessfully");
    }

    void NullDataFetching()
    {
        Debug.Log("Null/NO Data Fetching from Server...");
    }

    void ErrorDataFetching()
    {
        Debug.Log("Failed to Fetch Data From Server");
    }

    void OnUpdateStatSucessEvent()
    {
        Debug.Log("Stats Updated Succesfully");
    }

}

public struct PlayerData
{
    public string DisplayName;
    public string PlayerFabId;
    public int Position;
    public int Score;
    public LoginIdentityProvider origination;
}

