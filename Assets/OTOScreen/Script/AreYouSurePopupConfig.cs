﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AreYouSurePopupConfig
{
	public int id;
	public string[] are_you_sure_title_text;
	public bool back_to_oto_enabled;
	public string[] back_to_oto_screen_text;
	public bool purchase_offer_enabled;
	public string[] purchase_offer_text;
	public bool skip_to_app_enabled;
	public string[] skip_to_app_text;

}
