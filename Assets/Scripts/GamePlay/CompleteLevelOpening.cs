﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class CompleteLevelOpening : MonoBehaviour
{

    [Header("Button")]
    public GameObject FG;
    public GameObject Coins;
    public GameObject Next;
    public GameObject MainMenu;
    public GameObject Restart;
    public GameObject RemoveAds;
    public Text ScoreText;
    public GameObject Header;
    public Text Coinsearned;
    public Text TotalCoins;
    public int coins;
    public GameObject coinPrefab;
    public int time;
    public void OpenPannel()
    {
        
        Debug.Log("Open Panel of level complete/////////////////");
        FG.transform.DOLocalMoveX(0.1f, 0.3f).SetUpdate(true).OnComplete(delegate { 
        Coins.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBounce).OnComplete(delegate
        {
            CalculateCoins();
            //StartCoroutine(GenerateCoins());
        });
        });
    }
    void CalculateCoins()
    {

        //TotalCoins.text = ;
        GameManager.Instance.Gdata.Coins += GamePlayHandler.instance.coins;
        Coinsearned.text = GameManager.Instance.Gdata.Coins.ToString();
       
            Next.gameObject.transform.DOScale(1f, 0.25f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
            {
                MainMenu.gameObject.transform.DOScale(1f, 0.25f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
                {
                    Restart.gameObject.transform.DOScale(1f, 0.25f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
                    {
                        Next.transform.DOScale(.9f, 0.2f).SetLoops(-1, LoopType.Yoyo).SetUpdate(true).SetUpdate(true);
                        //GamePlayHandler.instance.Player.SetActive(false);
                        //if (!GameManager.Instance.Gdata.RemoveAds)
                        //{
                        //    RemoveAds.SetActive(true);
                        //    RemoveAds.transform.DOScale(1.1f, 0.5f).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);
                        //}
                        //ScoreText.text = GameManager.instance.Gdata.score.ToString();
                        MonetizationManager.instance.ShowInterstitialMediation();
                    });

                });
            });
       
    
}
   
}
