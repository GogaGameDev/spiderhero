﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameManager : MonoBehaviour
{
    #region Attributes
    public int LevelSelected = 0;
    public int FreeRoamSelected = 0;
    public int BikeSelected = 0;
    public int ModSelected = 0;
    public static GameManager Instance;
    public GameData Gdata;
    #endregion


    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ChangeScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }
 
}
