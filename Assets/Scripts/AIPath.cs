﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIPath : MonoBehaviour {

    public Color LineColor;
    private List<Transform> nodes = new List<Transform>();
    private int CurrentNode = 0;

    void OnDrawGizmos()
    {
        Gizmos.color = LineColor;
        Transform[] pathTransforms = GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != transform)
            {
                nodes.Add(pathTransforms[i]);
            }
        }

        for (int i = 0; i < nodes.Count; i++)
        {
            Vector3 CurrentNode = nodes[i].position;
            Vector3 PreviousNode = Vector3.zero;

            if (i > 0)
            {
                PreviousNode = nodes[i - 1].position;
            }
            else if (i == 0 && nodes.Count > 1)
            {
                PreviousNode = nodes[nodes.Count - 1].position;
            }


            Gizmos.DrawLine(PreviousNode, CurrentNode);
            Gizmos.DrawWireSphere(CurrentNode, 0.3f);
        }


    }
    
}
