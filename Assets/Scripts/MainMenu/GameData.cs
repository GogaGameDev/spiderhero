﻿using UnityEngine;

[CreateAssetMenu(fileName = "GameData", menuName = "Settings/GameData", order = 1)]
public class GameData : ScriptableObject
{
    public int LevelCompleted;
    public int TotalScores;
    public int Coins;
    public int Cash;
    public string PlayerName;
    public bool SetPlayerName;
    public bool RemoveAds;
    public bool UnLockedRickshaw2;
    public bool UnLockedRickshaw3;
}