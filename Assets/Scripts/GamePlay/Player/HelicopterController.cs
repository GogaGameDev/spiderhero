﻿using UnityEngine;
using UnityEngine.UI;

namespace NewHelicopter
{
    // USING
    // Input.GetAxis
    //
    public class HelicopterController : MonoBehaviour
    {

        private string horizontalAxis = "Horizontal";
        private string verticalAxis = "Vertical";
        private string jumpButton = "Jump";

        [Header("Inputs")]
        public bool isVirtualJoystick = false;

        [Header("View")]
        // to helicopter model
        public AudioSource HelicopterSound;
        public Rigidbody HelicopterModel;
        //public HeliRotorController MainRotorController;
        //public HeliRotorController SubRotorController;

        [Header("Fly Settings")]
        public float TurnForce = 1.3f;
        public float ForwardForce = 10f;
        public float ForwardTiltForce = 20f;
        public float TurnTiltForce = 15f;
        public float EffectiveHeight = 50f;

        public float turnTiltForcePercent = 1.5f;
        public float turnForcePercent = 1.3f;
        public float UpwardForce;
        public float DownwardForce;
        public float _engineForce;
        public float EngineForce
        {
            get { return _engineForce; }
            set
            {
                //MainRotorController.RotarSpeed = value * 80;
                //SubRotorController.RotarSpeed = value * 40;
                HelicopterSound.pitch = Mathf.Clamp(value / 40, 0, 1.2f);
                //if (UIGameController.runtime != null && UIGameController.runtime.EngineForceView != null)
                //    UIGameController.runtime.EngineForceView.text = string.Format("Engine value [ {0} ] ", (int)value);

                _engineForce = value;

            }
        }

        private Vector2 hMove = Vector2.zero;
        private Vector2 hTilt = Vector2.zero;
        private float hTurn = 0f;
        public bool IsOnGround = true;
        public float patrolRemaining;
        public float PatrolConsumption;
        public bool PatrolRefillNeed = false;
        public bool OutOfFuel = false;
        public bool EngineOn = false;
        public bool WatchTutorial = false;
        public bool DisactiveHandA = false;
        private void Start()
        {
            patrolRemaining = 100;
            if (patrolRemaining <= 10)
            {
                patrolRemaining = 15;
            }
            UImanager.instance.PatrolSlider.fillAmount = patrolRemaining / 100f;
            UImanager.instance.EngineBtn.GetComponent<Button>().onClick.AddListener(() => Engine());
            EngineOn = false;
            HelicopterSound.enabled = false;
        }
        private void Update()
        {


            if (patrolRemaining > 0 && EngineOn)
            {
                patrolRemaining -= Time.deltaTime * PatrolConsumption;
                UImanager.instance.PatrolSlider.fillAmount = patrolRemaining / 100f;
                Debug.Log("bbbbbbbbbbbbbbbbbb");
                if (patrolRemaining < 25 && !PatrolRefillNeed)
                {
                    Debug.Log("AAAAAAAAA");
                    //UImanager.instance.PatrolIcon.GetComponent<Animator>().enabled = true;
                    GamePlayHandler.instance.ActivePartolFIllingPoints();
                    PatrolRefillNeed = true;
                }
                patrolRemaining -= Time.deltaTime * PatrolConsumption;
                UImanager.instance.PatrolSlider.fillAmount = patrolRemaining / 100f;

            }
            else if (patrolRemaining <= 0 && !UImanager.instance.OutOfFuel)
            {
                HelicopterSound.pitch = 0;
                UImanager.instance.OutOfFuel = true;
                //UImanager.instance.Hand2.SetActive(true);
                //UImanager.instance.EngineBtn.GetComponent<Image>().overrideSprite = UImanager.instance.EngineOff;
                //UImanager.instance.PatrolBtn.interactable = true;
                Debug.Log("aaaaaaaaaaaaaaaaaaaaaaaaaa");
                UImanager.instance.OpenWatchVideoPanel();
                //this.GetComponent<RCC_CarControllerV3>().KillOrStartEngine();
                patrolRemaining = 0;
            }

        }
        void FixedUpdate()
        {
            if (patrolRemaining > 0 && EngineOn)
            {
                ProcessingInputs();
                LiftProcess();
                MoveProcess();
                TiltProcess();

                //UImanager.instance.PatrolIcon.GetComponent<Animator>().enabled = true;


            }

        }

        private void MoveProcess()
        {
            var turn = TurnForce * Mathf.Lerp(hMove.x, hMove.x * (turnTiltForcePercent - Mathf.Abs(hMove.y)), Mathf.Max(0f, hMove.y));
            hTurn = Mathf.Lerp(hTurn, turn, Time.fixedDeltaTime * TurnForce);
            HelicopterModel.AddRelativeTorque(0f, hTurn * HelicopterModel.mass, 0f);
            HelicopterModel.AddRelativeForce(Vector3.forward * Mathf.Max(0f, hMove.y * ForwardForce * HelicopterModel.mass));
        }

        private void LiftProcess()
        {
            var upForce = 1 - Mathf.Clamp(HelicopterModel.transform.position.y / EffectiveHeight, 0, 1);
            upForce = Mathf.Lerp(0f, EngineForce, upForce) * HelicopterModel.mass;
            HelicopterModel.AddRelativeForce(Vector3.up * upForce);
        }

        private void TiltProcess()
        {
            hTilt.x = Mathf.Lerp(hTilt.x, hMove.x * TurnTiltForce, Time.deltaTime);
            hTilt.y = Mathf.Lerp(hTilt.y, hMove.y * ForwardTiltForce, Time.deltaTime);
            HelicopterModel.transform.localRotation = Quaternion.Euler(hTilt.y, HelicopterModel.transform.localEulerAngles.y, -hTilt.x);
        }

        private void ProcessingMobileInputs()
        {
            if (!IsOnGround)
            {
                hMove.x = Input.GetAxis(horizontalAxis);
                hMove.y = Input.GetAxis(verticalAxis);
            }
            else
            {
                hMove.x = 0;
                hMove.y = 0;
            }

            if (Input.GetAxis(jumpButton) > 0 && EngineForce < 40)
            {
                EngineForce += UpwardForce;
            }
            else
            if (Input.GetAxis(jumpButton) < 0)
            {
                EngineForce -= DownwardForce;
            }
        }


        private void ProcessingInputs()
        {
            if (!IsOnGround)
            {
                hMove.x = GetInput(horizontalAxis);
                hMove.y = GetInput(verticalAxis);
                if (!DisactiveHandA)
                {
                    DisactiveHandA = true;
                    UImanager.instance.HandA.SetActive(false);
                }
            }

            if (GetInput(jumpButton) > 0 && EngineForce < 40)

                EngineForce += UpwardForce;
            else
            if (GetInput(jumpButton) < 0)
                EngineForce -= DownwardForce;

        }

        private float GetInput(string input)
        {
            if (isVirtualJoystick)
                return SimpleInput.GetAxis(input);
            else
                return Input.GetAxis(input);
        }


        private void OnCollisionEnter()
        {
            IsOnGround = true;
        }

        private void OnCollisionExit()
        {
            IsOnGround = false;
        }
        public void Engine()
        {
            if (!EngineOn)
            {
                UImanager.instance.Hand.SetActive(false);
                HelicopterSound.enabled = true;
                EngineOn = true;
                UImanager.instance.EngineBtn.GetComponent<Image>().overrideSprite = UImanager.instance.EngineOn;
                if (!WatchTutorial)
                {
                    WatchTutorial = true;
                    UImanager.instance.HandA.SetActive(true);
                }
            }
            else
            {
                HelicopterSound.enabled = false;
                EngineOn = false;
                UImanager.instance.EngineBtn.GetComponent<Image>().overrideSprite = UImanager.instance.EngineOff;
                EngineForce = 0;
            }
        }
    }
}