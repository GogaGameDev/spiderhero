﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Yodo1.MAS;
using OTO;
using System;

public class YodaAdManager : Singleton<YodaAdManager> {

    [SerializeField] bool _sendInsideFirebaseEvents = true;
    [SerializeField] bool _doNotDestroy = true;
    Action onRewardAction = null;
    Action OnRewardFailAction = null;
    Action OnRewardSkipAction = null;
    bool rewarded = false;
	// Use this for initialization
	void Start ()
	{
		Yodo1U3dMas.InitializeSdk();

        // init banner
        #region BANNER_INIT
        Yodo1U3dMas.SetBannerAdDelegate((Yodo1U3dAdEvent adEvent, Yodo1U3dAdError error) => {
            Debug.Log("[Yodo1 Mas] BannerdDelegate:" + adEvent.ToString() + "\n" + error.ToString());
            switch (adEvent)
            {
                case Yodo1U3dAdEvent.AdClosed:
                    Debug.Log("[Yodo1 Mas] Banner ad has been closed.");
                    if (FireBaseAnalitycs.instance != null && _sendInsideFirebaseEvents)
                    {
                        FireBaseAnalitycs.instance.OnBannerTap_Send();
                    }

                   
                    break;
                case Yodo1U3dAdEvent.AdOpened:
                    Debug.Log("[Yodo1 Mas] Banner ad has been shown.");
                    if (FireBaseAnalitycs.instance != null && _sendInsideFirebaseEvents)
                    {
                        FireBaseAnalitycs.instance.OnBannerAdShown_Send();
                    }
                    break;
                case Yodo1U3dAdEvent.AdError:
                    Debug.Log("[Yodo1 Mas] Banner ad error, " + error.ToString());
                    break;
            }
        });
        #endregion /BANNER_INIT

        #region Intersistial_Init
        Yodo1U3dMas.SetInterstitialAdDelegate((Yodo1U3dAdEvent adEvent, Yodo1U3dAdError error) => {
            Debug.Log("[Yodo1 Mas] InterstitialAdDelegate:" + adEvent.ToString() + "\n" + error.ToString());
            switch (adEvent)
            {
                case Yodo1U3dAdEvent.AdClosed:
                    Debug.Log("[Yodo1 Mas] Interstital ad has been closed.");
                    if (FireBaseAnalitycs.instance != null && _sendInsideFirebaseEvents)
                    {
                       // FireBaseAnalitycs.instance.OnInterstitialTap_Send();
                    }
                    break;
                case Yodo1U3dAdEvent.AdOpened:
                    Debug.Log("[Yodo1 Mas] Interstital ad has been shown.");
                    if (FireBaseAnalitycs.instance != null && _sendInsideFirebaseEvents) {
                        FireBaseAnalitycs.instance.OnIntersistialAdShown_Send();
                    }
                    break;
                case Yodo1U3dAdEvent.AdError:
                    Debug.Log("[Yodo1 Mas] Interstital ad error, " + error.ToString());
                    break;
            }
        });
        #endregion

        #region Rewarded_video_init
        Yodo1U3dMas.SetRewardedAdDelegate((Yodo1U3dAdEvent adEvent, Yodo1U3dAdError error) => {
            Debug.Log("[Yodo1 Mas] RewardVideoDelegate:" + adEvent.ToString() + "\n" + error.ToString());
            switch (adEvent)
            {
                case Yodo1U3dAdEvent.AdClosed:
                    if (!rewarded && OnRewardSkipAction != null)
                    {
                        OnRewardSkipAction.Invoke();
                    }
                    Debug.Log("[Yodo1 Mas] Reward video ad has been closed.");
                    break;
                case Yodo1U3dAdEvent.AdOpened:
                    Debug.Log("[Yodo1 Mas] Reward video ad has shown successful.");
                    if (FireBaseAnalitycs.instance != null && _sendInsideFirebaseEvents)
                    {
                        FireBaseAnalitycs.instance.OnRewardAdShown_Send();
                    }
                    break;
                case Yodo1U3dAdEvent.AdError:
                    Debug.Log("[Yodo1 Mas] Reward video ad error, " + error);
                    if (OnRewardFailAction != null)
                    {
                        OnRewardFailAction.Invoke();

                    }
                    break;
                case Yodo1U3dAdEvent.AdReward:
                    Debug.Log("[Yodo1 Mas] Reward video ad reward, give rewards to the player.");
                    rewarded = true;
                    if (onRewardAction != null)
                    {
                        onRewardAction.Invoke();
                    }
                    if (FireBaseAnalitycs.instance != null && _sendInsideFirebaseEvents)
                    {
                        FireBaseAnalitycs.instance.OnRewardAdCompleteInside_Send();
                    }
                    break;
            }

        });
        #endregion

        if (_doNotDestroy)
        {
            DontDestroyOnLoad(this);
        }
    }

    #region BANNER
    public bool IsBannerLoaded()
    {
        bool isLoaded = Yodo1U3dMas.IsBannerAdLoaded();
        return isLoaded;
    }

    public void ShowBanner()
    {
        if (!app_ads_flag.is_yodo_enabled) {
            return;
        }
        Yodo1U3dMas.ShowBannerAd();

    }

    public void ShowBanner(int align,int offsetX=0,int offsetY=0)
    {
        //for banner on bottom
      //  int align = Yodo1U3dBannerAlign.BannerBottom;
        //for banner on top
       // int align = Yodo1U3dBannerAlign.BannerTop;

        Yodo1U3dMas.ShowBannerAd(align);
    }
    public void CloseBanner()
    {
        Yodo1U3dMas.DismissBannerAd();

    }
    #endregion /BANNER
    #region INTERSISTIAL

    public bool IsInterstitalReady() {
        bool isLoaded = Yodo1U3dMas.IsInterstitialAdLoaded();

        return isLoaded;
    }

    public void ShowInterstital()
    {
        if (!app_ads_flag.is_yodo_enabled)
        {
            return;
        }
        Yodo1U3dMas.ShowInterstitialAd();

    }
    #endregion
    #region REWARDEDVIDEO
    public bool IsVideoReady()
    {
        bool isLoaded = Yodo1U3dMas.IsRewardedAdLoaded();

        return isLoaded;
    }
    public void ShowRewardedVideoAd(Action onReward=null,Action OnRewardSkip=null,Action OnRewardFail=null)
    {
        onRewardAction = onReward;
        OnRewardFailAction = OnRewardFail;
        OnRewardSkipAction = OnRewardSkip;

        if (!app_ads_flag.is_yodo_enabled)
        {
            return;
        }
        Yodo1U3dMas.ShowRewardedAd();

    }
    #endregion
}
