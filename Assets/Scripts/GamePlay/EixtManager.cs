﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class EixtManager : MonoBehaviour
{
    public GameObject ExitPanel;
    public GameObject FG;
    public GameObject Text;
    public GameObject Yes;
    public GameObject No;
    //int touchCount;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Application.platform == RuntimePlatform.Android)
        {
            if (Input.GetKey(KeyCode.Escape))
            {
                
                //MonetizationManager.instance.ShowInterstitialMediation();
                showExitPanel();

                
            }
        }
    }

    //public void ExitPanelHandling()
    //{
    //    if (touchCount % 2 == 0)
    //    {
    //        touchCount++;
    //        showExitPanel();
    //        MonetizationManager.instance.showInterstitialAd();
    //    }
    //    else if (touchCount % 2 == 1)
    //    {
    //        touchCount++;
    //        hideExitPanel();
    //    }
    //}


    public void showExitPanel()
    {
        MonetizationManager.instance.ShowAdmobInterstitialMediation();
        ExitPanel.SetActive(true);
        ExitPanel.transform.DOScale(1f, 0.1f).SetEase(Ease.OutBounce).OnComplete(delegate {
            FG.transform.DOLocalMoveX(0.1f, 0.3f).OnComplete(delegate
            {
                Text.transform.DOScale(1f, 0.2f).OnComplete(delegate
                {
                    Yes.transform.DOScale(1f, 0.2f).OnComplete(delegate
                    {
                        No.transform.DOScale(1f, 0.2f).OnComplete(delegate
                        {

                        });
                    });
                });
            });
        
        
        });
    }
    public void hideExitPanel()
    {
        ExitPanel.transform.DOScale(0f, 0.5f).SetEase(Ease.InBounce).SetUpdate(true).OnComplete(delegate
        {
            ExitPanel.SetActive(false);

        });
    }

    public void ExitYesBtn()
    {
#if UNITY_ANDROID
        Application.Quit();
#elif UNITY_IOS
        Application.Quit();
#endif
    }
    public void ExitNoBtn()
    {
        hideExitPanel();
    }
}
