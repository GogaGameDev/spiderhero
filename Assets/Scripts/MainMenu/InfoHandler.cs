﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class InfoHandler : MonoBehaviour
{
    public GameObject FG;
    public GameObject BackBtn;
    public GameObject Logo;
    
    public void OpenPanel()
    {
        FG.transform.DOLocalMoveX(0.1f, 0.3f).OnComplete(delegate
        {
            Logo.transform.DOScale(1, 0.2f).OnComplete(delegate
            {
                BackBtn.transform.DOScale(1, 0.2f).OnComplete(delegate
                {

                });
            });
        });
    }
    public void ClosePanel()
    {
        BackBtn.transform.DOScale(0, 0.2f).OnComplete(delegate
        {
            Logo.transform.DOScale(0, 0.2f).OnComplete(delegate
            {
                FG.transform.DOLocalMoveX(-1323f, 0.3f).OnComplete(delegate
                {
                    MainMenuHandler.Instace.InfoPanel.SetActive(false);
                });
                });
            });

    }
}
