﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RestorePurchase : MonoBehaviour
{
    Button restorePurchaseButton;
    void Start()
    {
        restorePurchaseButton = GetComponent<Button>();
        restorePurchaseButton.onClick.AddListener(Restore);
    }

   void Restore() {
       

        if (app_ads_flag.qonversion_enabled)
        {
             
               QonversionUnity.Qonversion.Restore((permissions, error) =>
                {
                    if (error == null)
                    {
                        // Handle permissions here
                        Debug.Log("Qonversion Restore Purchases Completed...");
                    }
                    else
                    {
                        // Handle the error  
                        Debug.Log("Error" + error.ToString());
                    }
                });
            
        }
        else
        {
            if (GleyEasyIAP.IAPManager.Instance!=null && GleyEasyIAP.IAPManager.Instance.IsInitialized())
            {
                GleyEasyIAP.IAPManager.Instance.RestorePurchases(ProductRestoredCallback);
               // Debug.Log("Restoring Purchases...");
            }
        }
     
    }
    private void ProductRestoredCallback(IAPOperationStatus status, string message, StoreProduct
product)
    {
        if (status == IAPOperationStatus.Success)
        {
            Debug.Log(" product restored: "+ product.GetStoreID());
        }
        else
        {
            //an error occurred in the buy process, log the message for more details
            Debug.Log("Buy product failed: " + message);
        }
    }
    
}
