﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audi_AIControl : MonoBehaviour
{
    private float maxSteerAngle = 50f;
    public float maxMotorTorque = 1500f;
    public float CurrentSpeed;
    public float topSpeed = 120f;
    public float maxBrakeTorque = 2500f;
    public bool isBraking = false;
 

    public Transform path;
    private List<Transform> nodes;
    public int currentNode = 0;
    
    private Rigidbody rb;
    public Vector3 COM = new Vector3(0, 0, 0);

    public WheelCollider FLWheelCollider;
    public WheelCollider FRWheelCollider;
    public WheelCollider BLWheelCollider;
    public WheelCollider BRWheelCollider;

    public GameObject FLWheel;
    public GameObject FRWheel;
    public GameObject BLWheel;
    public GameObject BRWheel;

    // reversing
    public bool IsReversing = false;
    public float ReverseCounter = 0f; // this is the time counter that will tell us after how much time vehicle will reverse
    public float WaitToReverse = 2f;
    public float ReverseFor = 4f;
    // respawning
    public float RespawnCounter = 2f;
    public float RespawnWait = 0f;



    //sensors
    [Header("Sensors")]
    public float sensorLength = 10f;
    public GameObject forntSensorPos;
    
    void Start()
    {

        rb = GetComponent<Rigidbody>();
        rb.centerOfMass = COM;
        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for (int i = 0; i < pathTransforms.Length; i++)
        {
            if (pathTransforms[i] != path.transform)
            {
                nodes.Add(pathTransforms[i]);
            }
        }
    }

    void Update()
    {
        WheelRotation();
       
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        Drive();
        CheckWaypointDistance();
        Sensor();
        applySteer();
        Braking();
        
        
        
    }
    
    private void Drive()
    {
        CurrentSpeed = Mathf.Round(2 * 22 / 7 * FLWheelCollider.radius * FLWheelCollider.rpm * 60 / 1000);
        if (CurrentSpeed < topSpeed && !isBraking)
        {
            if (!IsReversing) { 
            BLWheelCollider.motorTorque = maxMotorTorque;
            BRWheelCollider.motorTorque = maxMotorTorque;
            
            CurrentSpeed = Mathf.Round(CurrentSpeed);
            }
            else
            {
                BLWheelCollider.motorTorque = -maxMotorTorque;
                BRWheelCollider.motorTorque = -maxMotorTorque;
                CurrentSpeed = Mathf.Round(CurrentSpeed);
            }
        }
        else
        {
            BLWheelCollider.motorTorque = 0;
            BRWheelCollider.motorTorque = 0;
        }
    }
    private void applySteer()
    {



        
        Vector3 relativeVector = transform.InverseTransformPoint(nodes[currentNode].position);
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
        FLWheelCollider.steerAngle = newSteer;
        FRWheelCollider.steerAngle = newSteer;
      


    }
    private void CheckWaypointDistance()
    {
        if(Vector3.Distance(transform.position , nodes[currentNode].position) < 0.5f){
            if (currentNode == nodes.Count - 1)
            {
                currentNode = 0;
               
            }
            else
            {
                currentNode++;
               
            }
        }
    }

    public void WheelRotation()
    {
        Quaternion flq;
        Vector3 flv;
        FLWheelCollider.GetWorldPose(out flv, out flq);
        FLWheel.transform.position = flv;
        FLWheel.transform.rotation = flq;

        Quaternion frq;
        Vector3 frv;
        FRWheelCollider.GetWorldPose(out frv, out frq);
        FRWheel.transform.position = frv;
        FRWheel.transform.rotation = frq;

        Quaternion blq;
        Vector3 blv;

        BLWheelCollider.GetWorldPose(out blv, out blq);
        BLWheel.transform.position = blv;
        BLWheel.transform.rotation = blq;

        Quaternion brq;
        Vector3 brv;

        BRWheelCollider.GetWorldPose(out brv, out brq);
        BRWheel.transform.position = brv;
        BRWheel.transform.rotation = brq;

    }
    public void Braking()
    {
        if (isBraking)
        {
            BLWheelCollider.brakeTorque = maxBrakeTorque;
            BRWheelCollider.brakeTorque = maxBrakeTorque;
          
        }
        else
        {
            BLWheelCollider.brakeTorque = 0;
            BRWheelCollider.brakeTorque = 0;
           
        }
    }
    public void Sensor()
    {
        RaycastHit hit;
        Ray ray = new Ray(forntSensorPos.transform.position, forntSensorPos.transform.forward);
        Debug.DrawRay(forntSensorPos.transform.position, forntSensorPos.transform.forward * 10, Color.red);
        if(Physics.Raycast(ray, out hit , 10))
        {
            BLWheelCollider.brakeTorque = maxBrakeTorque;
            BRWheelCollider.brakeTorque = maxBrakeTorque;
            //Debug.DrawLine(SensorStartPos, hit.point);
        }
        else
        {

            BLWheelCollider.brakeTorque = 0;
            BRWheelCollider.brakeTorque = 0;
        }


    }//Center Sensor

        














}