﻿using UnityEngine;
using System.Collections;
//using GoogleMobileAds.Api;
//using GoogleMobileAds;
using System;
using UnityEngine.UI;
using UnityEngine.Advertisements;
using OTO;

public class MonetizationManager : MonoBehaviour
{


    //Banner Object
    //private BannerView bannerAd;
    //Banner positions Enumarator
    public enum BannerAdPos
    {
        Bottom,
        BottomLeft,
        BottomRight,
        Center,
        Top,
        TopLeft,
        TopRight
    };
    public BannerAdPos BannerAdPosition;
   // private AdPosition BannerAdPOSITIONS;

    //Rewarded Video Object
    //private RewardBasedVideoAd rewardBasedVideoAd;

    //Interstitial Ad Object
   // public InterstitialAd interstitial;

    public static MonetizationManager instance;

    //public Text dummytext;


    public GameData GData;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (instance == null)
        {
            instance = this;
        }

    }

    // Use this for initialization
    void Start()
    {
        AssignBannerPoitions();
   


       
    }
    public void ShowUnityBanner()
    {
        if (GData.RemoveAds)
        {
            return;
        }

            if (app_ads_flag.is_yodo_enabled && !app_ads_flag.Hide_Ads)
        {
            YodaAdManager.Instance.ShowBanner(Yodo1.MAS.Yodo1U3dBannerAlign.BannerBottom);
        }
    }
   
    public void HideUnityBanner()
    {
        YodaAdManager.Instance.CloseBanner();
    }
    void AssignBannerPoitions()
    {
        
        
    }


    public void showInterstitialAd()
    {

        if (GData.RemoveAds)
        {
            return;
        }

        if (!app_ads_flag.is_admob_enabled || app_ads_flag.Hide_Ads) {
            return;
        }
        //Show Ad
        YodaAdManager.Instance.ShowInterstital();

    }


    //Intersititial Call Backs



    //BannerAd Methods

    public void showBannerAd()
    {
        if (GData.RemoveAds)
        {
            return;
        }

        if (!app_ads_flag.is_yodo_enabled || app_ads_flag.Hide_Ads)
        {
            return;
        }

        YodaAdManager.Instance.ShowBanner();



    }


    public void HideBannerAd()
    {
        YodaAdManager.Instance.CloseBanner();

    }


    public void ShowRewardeVideoAd()
    {
        if (!app_ads_flag.is_yodo_enabled || app_ads_flag.Hide_Ads)
        {
            return;
        }

        YodaAdManager.Instance.ShowRewardedVideoAd(RewardedVideoComplete);
    }

    //This method is Automatically called when rewarded video is completed sucessfully
    public void RewardedVideoComplete()
    {
        //	dummytext.text = "Rewarded";
        UImanager.instance.WatchAdSuccessfully.Invoke();

    }


  
    /*
    public void HandleRewardBasedVideoRewarded(object sender, Reward args)
    {
        string type = args.Type;
        double amount = args.Amount;
        MonoBehaviour.print(
        "HandleRewardBasedVideoRewarded event received for " + amount.ToString() + " " + type);
        //Passing the name of method which is call when rewarded video is completed.
       // BroadcastMessage("RewardedVideoComplete");

        
    }
    */
 
 


    public void ShowUnityInterstitial()
    {
        if (GData.RemoveAds)
        {
            return;
        }

        if (!app_ads_flag.is_yodo_enabled || app_ads_flag.Hide_Ads)
        {
            return;
        }

        YodaAdManager.Instance.ShowInterstital();
       
    }


    public void ShowUnityRewardedVideo()
    { 
        if (!app_ads_flag.is_yodo_enabled|| app_ads_flag.Hide_Ads)
        {
            return;
        }

        YodaAdManager.Instance.ShowRewardedVideoAd(()=> {
            UImanager.instance.WatchAdSuccessfully.Invoke();

        });
      
    }

   

    public void ShowInterstitialMediation()
    {
        if (!GData.RemoveAds)
        {
            if (!app_ads_flag.is_yodo_enabled || app_ads_flag.Hide_Ads) {
                return;
            }

            YodaAdManager.Instance.ShowInterstital();
        }
    }

    public void ShowAdmobInterstitialMediation()
    {
        if (!GData.RemoveAds)
        {
            if (!app_ads_flag.is_yodo_enabled || app_ads_flag.Hide_Ads)
            {
                return;
            }

            YodaAdManager.Instance.ShowInterstital();
        }
    }

    public void ShowRewardedVideoMediation()
    {
        ShowUnityRewardedVideo();
    }

}
