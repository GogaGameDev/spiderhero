﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NewHelicopter;
public class PlayerManager : MonoBehaviour
{

    public GameObject DropOffLocation;
    public GameObject PickUpPoint;
    public Transform PassSitPos;
    public int CurrentPas = 0;
    public GameData Gdata;
    public Transform CoinsPosNew;
    public float time;
    public GameObject CoinsPrefab;
    public bool LevelCOmplete = false;
    private void Start()
    {
        
        if(GameManager.Instance.LevelSelected == 0 && GameManager.Instance.Gdata.LevelCompleted <= 0)
        {
            //GameManager.Instance.Gdata.Coins = 10;
            UImanager.instance.coins.text = GameManager.Instance.Gdata.Coins.ToString();
        }
        UImanager.instance.DropOffBtn.GetComponent<Button>().onClick.AddListener(() => DropPassanger());
        UImanager.instance.PickPasBtn.GetComponent<Button>().onClick.AddListener(() => PickPassanger());
        //UImanager.instance.PatrolBtn.GetComponent<Button>().onClick.AddListener(() => WatchAd());
    }
    
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Partol")
        {
            switch (GameManager.Instance.ModSelected)
            {
                case 0:
                        if (Gdata.Coins > 5)
                        {
                            //UImanager.instance.PatrolIcon.GetComponent<Animator>().enabled = false;
                            GamePlayHandler.instance.DisactivePartolFIllingPoints();
                            UImanager.instance.Coinsdetection();
                            GamePlayHandler.instance.ActiveDropOffLocation();
                            //other.gameObject.SetActive(false);
                            GetComponent<HelicopterController>().patrolRemaining = 100f;
                            UImanager.instance.PatrolSlider.fillAmount = GetComponent<HelicopterController>().patrolRemaining / 100f;
                           
                        }
                        else
                        {
                            UImanager.instance.ShowAlert();
                        }
                    
                    break;
                case 1:
                    if(Gdata.Coins > 5)
                    {
                        GamePlayHandler.instance.DisactivePartolFIllingPoints();
                        UImanager.instance.Coinsdetection();
                        GetComponent<HelicopterController>().patrolRemaining = 100f;
                        UImanager.instance.PatrolSlider.fillAmount = GetComponent<HelicopterController>().patrolRemaining / 100f;
                        UImanager.instance.PatrolIcon.GetComponent<Animator>().enabled = false;
                    }
                    else
                    {
                        UImanager.instance.ShowAlert();

                    }
                    break;
            }
            
        }
        else if(other.tag == "Pick")
        {
            PickUpPoint = other.gameObject;
            UImanager.instance.PickPasBtn.SetActive(true);
        }
        else if (other.tag == "Drop")
        {
            switch (GameManager.Instance.ModSelected) {

                case 0:
                if(GameManager.Instance.LevelSelected > 0)
            {
                DropOffLocation = other.gameObject;
                UImanager.instance.DropOffBtn.SetActive(true);

            }
            else
            {
                DropOffLocation = other.gameObject;
                other.gameObject.SetActive(false);
                UImanager.instance.OnLevelComplete.Invoke();
                
            }
                    break;
                case 1:
                    DropOffLocation = other.gameObject;
                    UImanager.instance.DropOffBtn.SetActive(true);

                    break;
        }
    }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Pick")
        {
            //PickUpPoint = null;
            UImanager.instance.PickPasBtn.SetActive(false);
        }
        else if (other.tag == "Drop")
        {
            //DropOffLocation = null;
            UImanager.instance.DropOffBtn.SetActive(false);

        }
    }
        public void PickPassanger()
    {
        StartCoroutine(StartFadeforPassPick());
    }
    IEnumerator StartFadeforPassPick()
    {
        UImanager.instance.FadeImgPanel.SetActive(true);
        GamePlayHandler.instance.PlayerCanvas.enabled = false;
        UImanager.instance.FadeImgPanel.GetComponent<Image>().canvasRenderer.SetAlpha(0);
        UImanager.instance.FadeImgPanel.GetComponent<Image>().CrossFadeAlpha(1,3,false);
        yield return new WaitForSeconds(3);
        switch (GameManager.Instance.ModSelected)
        {
            case 0:
        GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers[GamePlayHandler.instance.currentPickPoint].transform.position = PassSitPos.transform.position;
        GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers[GamePlayHandler.instance.currentPickPoint].transform.rotation = PassSitPos.transform.rotation;
                GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers[GamePlayHandler.instance.currentPickPoint].GetComponent<Animator>().SetTrigger("Sit");

                GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers[GamePlayHandler.instance.currentPickPoint].transform.parent = PassSitPos;
                break;
            case 1:
                GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].Passangers[GamePlayHandler.instance.RandNo].transform.position = PassSitPos.transform.position;
                GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].Passangers[GamePlayHandler.instance.RandNo].transform.rotation = PassSitPos.transform.rotation;
                GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].Passangers[GamePlayHandler.instance.RandNo].transform.parent = PassSitPos;
                break;
        }
        GamePlayHandler.instance.ActiveDropOffLocation();
        UImanager.instance.PickPasBtn.SetActive(false);
        CurrentPas++;
        PickUpPoint.SetActive(false);
        if (CurrentPas >= 1)
        {
            GamePlayHandler.instance.DropOffLocation.GetComponent<EnemyDirection>().FindTarget = false;

        }
        yield return new WaitForSeconds(1);
        GamePlayHandler.instance.PlayerCanvas.enabled = true;
        UImanager.instance.FadeImgPanel.SetActive(false);

    }
    public void DropPassanger()
    {
        StartCoroutine(FadeForPassDrop());
    }
    IEnumerator FadeForPassDrop()
    {
        UImanager.instance.FadeImgPanel.SetActive(true);
        GamePlayHandler.instance.PlayerCanvas.enabled = false;
        UImanager.instance.FadeImgPanel.GetComponent<Image>().canvasRenderer.SetAlpha(0);
        UImanager.instance.FadeImgPanel.GetComponent<Image>().CrossFadeAlpha(1, 3, false);
        yield return new WaitForSeconds(3);
        UImanager.instance.DropOffBtn.SetActive(false);
        switch (GameManager.Instance.ModSelected) {
            case 0:
                UImanager.instance.TotalPass--;
                UImanager.instance.NumberOfPass.text = UImanager.instance.TotalPass.ToString();
        GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers[GamePlayHandler.instance.currentPickPoint].transform.parent = null;
        GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers[GamePlayHandler.instance.currentPickPoint].SetActive(false);
        GamePlayHandler.instance.currentPickPoint++;
                int coins = Random.Range(1500, 2000);
                GamePlayHandler.instance.coins += coins;
                UImanager.instance.coins.text = GamePlayHandler.instance.coins.ToString();
                if (GamePlayHandler.instance.currentPickPoint >= GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers.Length)
        {
            DropOffLocation.SetActive(false);
            Debug.Log("Level Completed............");
            UImanager.instance.OnLevelComplete.Invoke();
            yield return new WaitForSeconds(1);
            UImanager.instance.FadeImgPanel.SetActive(false);
            //StartCoroutine(GenerateCoins());
                    LevelCOmplete = true;
        }
                else
        {
            DropOffLocation.SetActive(false);
            //GamePlayHandler.instance.currentPickPoint++;
            GamePlayHandler.instance.ActivePickUpLocation();
            GamePlayHandler.instance.PickUpLocation.GetComponent<EnemyDirection>().FindTarget = false;
            Debug.Log("Find new target");
            yield return new WaitForSeconds(1);
            GamePlayHandler.instance.PlayerCanvas.enabled = true;
            UImanager.instance.FadeImgPanel.SetActive(false);
        }
                break;
            case 1:
                GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers[GamePlayHandler.instance.RandNo].transform.parent = null;
                GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers[GamePlayHandler.instance.RandNo].SetActive(false);
                GamePlayHandler.instance.ActivePickUpLocation();
                GamePlayHandler.instance.PickUpLocation.GetComponent<EnemyDirection>().FindTarget = false;
                GamePlayHandler.instance.PlayerCanvas.enabled = true;
                UImanager.instance.FadeImgPanel.SetActive(false);
                DropOffLocation.SetActive(false);
                int coinsearned = Random.Range(1500, 2000);
                GamePlayHandler.instance.coins += coinsearned;
                Gdata.Coins += GamePlayHandler.instance.coins;
                PlayFabManager.Instance.SendGameData();
                UImanager.instance.coins.text = Gdata.Coins.ToString();
                break;
    }
}
    IEnumerator GenerateCoins()
    {
        for (int i = 0; i < GamePlayHandler.instance.coins; i++)
        {
            var Canvas = GameObject.Find("RCCCanvas").transform;
            Debug.Log(i);
            yield return new WaitForSeconds(0.2f);
            GameManager.Instance.Gdata.Coins--;
            UImanager.instance.coins.text = GameManager.Instance.Gdata.Coins.ToString();
            var go = Instantiate(CoinsPrefab, CoinsPosNew.position, Quaternion.identity, Canvas);
            //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.Chips);
            Destroy(go, time + 0.7f);
        }
    }
    public void WatchAd()
    {
        UImanager.instance.AdforPatrol = true;
        UImanager.instance.OpenWatchVideoPanel();
    }
   
}
