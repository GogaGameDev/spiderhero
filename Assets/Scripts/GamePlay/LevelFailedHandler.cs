﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class LevelFailedHandler : MonoBehaviour
{

    [Header("Button")]
    public GameObject FG;
    public GameObject Coins;
    public GameObject MainMenu;
    public GameObject Restart;
    public GameObject RemoveAds;
    public Text ScoreText;
    public GameObject Header;
    public Text Coinsearned;
    public int coins;

    public void OpenPannel()
    {

        Debug.Log("Open Panel of level failed");
        FG.transform.DOLocalMoveX(0.1f, 0.3f).SetUpdate(true).OnComplete(delegate {
            Debug.Log("Moving FG");
            Coins.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
            {
                CalculateCoins();
                //StartCoroutine(GenerateCoins());
            });
        });
    }
    void CalculateCoins()
    {
        
        GameManager.Instance.Gdata.Coins += coins;
        //TotalCoins.text = GameManager.Instance.Gdata.Coins.ToString();
        Coinsearned.text = coins.ToString();
        //PlayFabManager.Instance.SendGameData();
       
                MainMenu.gameObject.transform.DOScale(1f, 0.25f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
                {
                    Restart.gameObject.transform.DOScale(1f, 0.25f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
                    {
                      
                        //GamePlayHandler.instance.Player.SetActive(false);
                        if (!GameManager.Instance.Gdata.RemoveAds)
                        {
                            //RemoveAds.SetActive(true);
                            //RemoveAds.transform.DOScale(1.1f, 0.5f).SetLoops(-1, LoopType.Yoyo);
                        }
                        //ScoreText.text = GameManager.instance.Gdata.score.ToString();
                        MonetizationManager.instance.ShowInterstitialMediation();
                    });

                });
        

    }


}
