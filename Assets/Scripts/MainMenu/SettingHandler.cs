﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
public class SettingHandler : MonoBehaviour
{
    public GameObject FG;
    public GameObject Header;
    public GameObject SoundsOnOff;
    public GameObject musicOnOff;
    public GameObject Info;


    public void OpenSetting()
    {
        FG.transform.DOScale(1f, 0.4f).OnComplete(delegate
        {
           
                SoundsOnOff.transform.DOScale(1f, 0.2f).OnComplete(delegate
                {
                    musicOnOff.transform.DOScale(1f, 0.2f).OnComplete(delegate
                    {
                        Info.transform.DOScale(1f, 0.2f).OnComplete(delegate
                        {

                        });
                });
            });
        });

    }
    public void CloseSetting()
    {
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        Info.transform.DOScale(0f, 0.2f).OnComplete(delegate {
            musicOnOff.transform.DOScale(0f, 0.2f).OnComplete(delegate
            {
                    SoundsOnOff.transform.DOScale(0f, 0.2f).OnComplete(delegate
                    {
                        FG.transform.DOScale(0f, 0.2f).OnComplete(delegate
                        {
                            MainMenuHandler.Instace.SettingPanel.SetActive(false);
                        
                        });
                        });
                    });
                });
            
        

    }

}
