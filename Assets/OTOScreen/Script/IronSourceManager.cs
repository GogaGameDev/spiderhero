﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace OTO
{
    public class IronSourceManager : Singleton<IronSourceManager>
    {

        /*

        [SerializeField] string appKey = "";
        Delegate OnReward = null;
        void Start()
        {
            //Invoke("Init", 3f);
            Init();
        }

        void Init() {
            IronSource.Agent.validateIntegration();
            IronSource.Agent.init(appKey);
            // 
            // IronSource.Agent.shouldTrackNetworkState(true);

            InitRewardedVideo();
            InitInterstitial();
            InitBanner();

            DontDestroyOnLoad(this.gameObject);
        }

    #region RewardedAd
        void InitRewardedVideo()
        {
            IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
            IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
            IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
            IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
            IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
            IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
        }
        public void ShowRewardedVideoAd(Delegate onReward) {
            OnReward = onReward;
            bool available = IronSource.Agent.isRewardedVideoAvailable();
            if (available) {
                IronSource.Agent.showRewardedVideo();
            }
        }
        public bool IsRewardVideoReady() {
            return IronSource.Agent.isRewardedVideoAvailable();
        }
        void OnApplicationPause(bool isPaused)
        {
            IronSource.Agent.onApplicationPause(isPaused);
        }
    #region RewardedVideo Events
        //Invoked when the RewardedVideo ad view has opened.
        //Your Activity will lose focus. Please avoid performing heavy 
        //tasks till the video ad will be closed.
        void RewardedVideoAdOpenedEvent()
        {
        }
        //Invoked when the RewardedVideo ad view is about to be closed.
        //Your activity will now regain its focus.
        void RewardedVideoAdClosedEvent()
        {
        }
        //Invoked when there is a change in the ad availability status.
        //@param - available - value will change to true when rewarded videos are available. 
        //You can then show the video by calling showRewardedVideo().
        //Value will change to false when no videos are available.
        void RewardedVideoAvailabilityChangedEvent(bool available)
        {
            //Change the in-app 'Traffic Driver' state according to availability.
            bool rewardedVideoAvailability = available;
        }

        //Invoked when the user completed the video and should be rewarded. 
        //If using server-to-server callbacks you may ignore this events and wait for 
        // the callback from the  ironSource server.
        //@param - placement - placement object which contains the reward data
        void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
        {
            if (OnReward != null) {
                OnReward.DynamicInvoke();
            }
        }
        //Invoked when the Rewarded Video failed to show
        //@param description - string - contains information about the failure.
        void RewardedVideoAdShowFailedEvent(IronSourceError error)
        {
        }

        // ----------------------------------------------------------------------------------------
        // Note: the events below are not available for all supported rewarded video ad networks. 
        // Check which events are available per ad network you choose to include in your build. 
        // We recommend only using events which register to ALL ad networks you include in your build. 
        // ----------------------------------------------------------------------------------------

        //Invoked when the video ad starts playing. 
        void RewardedVideoAdStartedEvent()
        {
        }
        //Invoked when the video ad finishes playing. 
        void RewardedVideoAdEndedEvent()
        {
        }
        //Invoked when the video ad is clicked. 
        void RewardedVideoAdClickedEvent(IronSourcePlacement placement)
        {
        }
    #endregion
    #endregion /RewardedAd
    #region IntersistialAd
        void InitInterstitial() {
            IronSourceEvents.onInterstitialAdReadyEvent += InterstitialAdReadyEvent;
            IronSourceEvents.onInterstitialAdLoadFailedEvent += InterstitialAdLoadFailedEvent;
            IronSourceEvents.onInterstitialAdShowSucceededEvent += InterstitialAdShowSucceededEvent;
            IronSourceEvents.onInterstitialAdShowFailedEvent += InterstitialAdShowFailedEvent;
            IronSourceEvents.onInterstitialAdClickedEvent += InterstitialAdClickedEvent;
            IronSourceEvents.onInterstitialAdOpenedEvent += InterstitialAdOpenedEvent;
            IronSourceEvents.onInterstitialAdClosedEvent += InterstitialAdClosedEvent;

            LoadInterstitial();
        }
        void LoadInterstitial() {
            IronSource.Agent.loadInterstitial();
        }
        public bool IsInterstitialReady() {
            return IronSource.Agent.isInterstitialReady();
        }
        public void ShowInterstitial() {
            IronSource.Agent.showInterstitial();
        }
    #region InterstitialEvents
        //Invoked when the initialization process has failed.
        //@param description - string - contains information about the failure.
        void InterstitialAdLoadFailedEvent(IronSourceError error)
        {
        }
        //Invoked when the ad fails to show.
        //@param description - string - contains information about the failure.
        void InterstitialAdShowFailedEvent(IronSourceError error)
        {
        }
        // Invoked when end user clicked on the interstitial ad
        void InterstitialAdClickedEvent()
        {
        }
        //Invoked when the interstitial ad closed and the user goes back to the application screen.
        void InterstitialAdClosedEvent()
        {
            IronSource.Agent.loadInterstitial();
        }
        //Invoked when the Interstitial is Ready to shown after load function is called
        void InterstitialAdReadyEvent()
        {
        }
        //Invoked when the Interstitial Ad Unit has opened
        void InterstitialAdOpenedEvent()
        {
        }
        //Invoked right before the Interstitial screen is about to open. NOTE - This event is available only for some of the networks. 
        // You should treat this event as an interstitial impression, but rather use InterstitialAdOpenedEvent
        void InterstitialAdShowSucceededEvent()
        {
        }
    #endregion
    #endregion /IntersistialAd
    #region BannerAd

        void InitBanner() {
            IronSourceEvents.onBannerAdLoadedEvent += BannerAdLoadedEvent;
            IronSourceEvents.onBannerAdLoadFailedEvent += BannerAdLoadFailedEvent;
            IronSourceEvents.onBannerAdClickedEvent += BannerAdClickedEvent;
            IronSourceEvents.onBannerAdScreenPresentedEvent += BannerAdScreenPresentedEvent;
            IronSourceEvents.onBannerAdScreenDismissedEvent += BannerAdScreenDismissedEvent;
            IronSourceEvents.onBannerAdLeftApplicationEvent += BannerAdLeftApplicationEvent;
        }
        public void LoadBanner(IronSourceBannerPosition position) {
            IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, position);
        }
        public void HideBanner() {
            IronSource.Agent.hideBanner();
        }
        public void DisplayBanner() {
            IronSource.Agent.displayBanner();
        }
        public void DestroyBanner() {
            IronSource.Agent.destroyBanner();
        }
    #region BannerEvents
        //Invoked once the banner has loaded
        void BannerAdLoadedEvent()
        {
        }
        //Invoked when the banner loading process has failed.
        //@param description - string - contains information about the failure.
        void BannerAdLoadFailedEvent(IronSourceError error)
        {
        }
        // Invoked when end user clicks on the banner ad
        void BannerAdClickedEvent()
        {
        }
        //Notifies the presentation of a full screen content following user click
        void BannerAdScreenPresentedEvent()
        {
        }
        //Notifies the presented screen has been dismissed
        void BannerAdScreenDismissedEvent()
        {
        }
        //Invoked when the user leaves the app
        void BannerAdLeftApplicationEvent()
        {
        }
        #endregion /BannerEvents
        #endregion


       */

        // for disabled

        public bool IsInterstitialReady()
        {

            return false;
        }
        public void ShowInterstitial()
        {

        }

        public bool IsRewardVideoReady()
        {
            return false;
        }

        public void ShowRewardedVideoAd(Delegate action)
        {

        }


    }
}
