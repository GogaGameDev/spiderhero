using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class EnemyDirection : MonoBehaviour
{
    public RectTransform WayPointPrefab;
    public RectTransform WayPoint;
    public Text Distance;
    private Transform Player;
    public GameObject Cam;
    private Vector3 offSet = new Vector3(0f, 1f, 0f);
    public bool IsIndicatorOffTheScreen;
    public bool FindTarget = true;
    // Start is called before the first frame update
    void Start()
    {
        FindTarget = true;
        Cam = GamePlayHandler.instance.Camera.transform.GetChild(0).gameObject;
        IsIndicatorOffTheScreen = false;
        Player = GamePlayHandler.instance.Player[GameManager.Instance.BikeSelected].transform;
        var Canvas = GameObject.Find("Waypoints").transform;
        WayPoint = Instantiate(WayPointPrefab, Canvas);
        Distance = WayPoint.GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!FindTarget)
        {
            Cam = GamePlayHandler.instance.Camera.transform.GetChild(0).gameObject;
            IsIndicatorOffTheScreen = false;
            Player = GamePlayHandler.instance.Player[GameManager.Instance.BikeSelected].transform;
            var Canvas = GameObject.Find("Waypoints").transform;
            WayPoint = Instantiate(WayPointPrefab, Canvas);
            Distance = WayPoint.GetComponentInChildren<Text>();
            FindTarget = true;
            Debug.Log("In Update of Finding Target");
        }
        Player = GamePlayHandler.instance.Player[GameManager.Instance.BikeSelected].transform; 
        float MinX = WayPoint.GetComponentInChildren<Image>().GetPixelAdjustedRect().width / 2;
        float MaxX = Screen.width - MinX;
        float MinY = WayPoint.GetComponentInChildren<Image>().GetPixelAdjustedRect().height / 2;
        float MaxY = Screen.width - MinY;
        Vector3 Pos = Cam.GetComponent<Camera>().WorldToScreenPoint(transform.position + offSet);
        Pos.x = Mathf.Clamp(Pos.x, MinX, MaxX);
        Pos.y = Mathf.Clamp(Pos.y, MinY, MaxY);
        if (!IsIndicatorOffTheScreen) { 
        WayPoint.gameObject.SetActive(Pos.z > 0);
        }
        //if (Vector3.Dot((transform.position - Camera.main.transform.position), transform.forward) < 0)
        //{
        //    if (Pos.x < Screen.width / 2)
        //    {
        //        Debug.Log(Pos.x);
        //        Pos.x = MaxX;
        //    }
        //    else
        //    {
        //        Debug.Log(Pos.x);
        //        Pos.x = MinX;
        //    }
        //}
        WayPoint.position = Pos;
        Distance.text = Vector3.Distance(transform.position, Player.position).ToString("0") + "m";
        if (Vector3.Distance(transform.position, Player.position) <10 && IsIndicatorOffTheScreen == false)
        {
            IsIndicatorOffTheScreen = true;
            WayPoint.gameObject.SetActive(false);
        }

        
    }
}
