﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.Linq;
using QonversionUnity;
using OTO;
public class IAP : MonoBehaviour
{
	public static IAP _instance;

	[HideInInspector] public UnityEvent OnPurchase;
	[HideInInspector] public UnityEvent OnInitFail;
	[HideInInspector] public UnityEvent OnInitSucess;
	[HideInInspector] public UnityEvent OnPurchaseChecked;
	[HideInInspector] public UnityEvent OnPurchaseUnchecked;
	bool _check;
	[SerializeField] bool doNotDestroy;

	//qonversion
	[SerializeField] IAPManagerType iapManagerType = IAPManagerType.Qonversion;
	[SerializeField] string m_ApplicationAccessKey;
	[SerializeField] bool m_ObserverMode = true;
	string currentBuingProductID = "";
	//Regular
	[HideInInspector] public ShopProductNames regular;
	public string regularID;
	//public string checkedID;
	//public string uncheacked;
	[HideInInspector] public EventString OnRegularBuy= new EventString();

    private void Awake()
    {
	
		if (iapManagerType == IAPManagerType.Qonversion)
		{
#if UNITY_EDITOR
			Qonversion.SetDebugMode();
#endif
			Qonversion.Launch(m_ApplicationAccessKey, m_ObserverMode);

		}
		if (m_ObserverMode)
		{
			Qonversion.Launch(m_ApplicationAccessKey, m_ObserverMode);

		}
		_instance = this;
		if (doNotDestroy) {
			DontDestroyOnLoad(this.gameObject);
		}
    }

	public void BuyIAP(ShopProductNames product, bool check)
	{
		_check = check;
		switch (iapManagerType)
		{
			case IAPManagerType.Qonversion:
				string productId = product.ToString();
				currentBuingProductID = productId;
				Qonversion.Purchase(productId, ProductBoughtCallback);
				break;
			case IAPManagerType.Regular:
				GleyEasyIAP.IAPManager.Instance.BuyProduct(product, ProductBoughtCallback);
				break;
		}
	}
	public void BuyIAP(string product, bool check)
	{
		_check = check;
		switch (iapManagerType)
		{
			case IAPManagerType.Qonversion:
				string productId = product;
				currentBuingProductID = productId;
				Qonversion.Purchase(productId, ProductBoughtCallback);
				break;
			case IAPManagerType.Regular:
				//IAPManager.Instance.BuyProduct(product, ProductBoughtCallback);
				break;
		}
	}
	public void BuyIAPRegular() {

		switch (iapManagerType)
		{
			case IAPManagerType.Qonversion:
				string productId = regularID;
				currentBuingProductID = productId;
				Qonversion.Purchase(productId, ProductBoughtCallback);
				break;
			case IAPManagerType.Regular:
				GleyEasyIAP.IAPManager.Instance.BuyProduct(regular, ProductBoughtCallback);
				break;
		}
	}



	public void Init() {
		switch (iapManagerType)
		{
			case IAPManagerType.Qonversion:
				Qonversion.Products(InitializeResultCallback);
				app_ads_flag.qonversion_enabled = true;
#if UNITY_EDITOR
				OnInitSucess.Invoke();
#endif
				break;
			case IAPManagerType.Regular:
				GleyEasyIAP.IAPManager.Instance.InitializeIAPManager(InitializeResultCallback);
				app_ads_flag.qonversion_enabled = false;
				break;
		}
	}

	private void InitializeResultCallback(IAPOperationStatus status, string message, List<StoreProduct>
shopProducts)
	{
		Debug.Log("IAP Status" + status);
		if (status == IAPOperationStatus.Success)
		{
			OnInitSucess.Invoke();
			//IAP was successfully initialized
			//loop through all products
			//Debug.Log("Product Count: " + shopProducts.Count);
			for (int i = 0; i < shopProducts.Count; i++)
			{
				
				//Debug.Log("Product ID: " + shopProducts[i].GetStoreID());
			}
			
		}
		else
		{
			Debug.Log("Error occurred "+message);
			OnInitFail.Invoke();
		}
	}
	
	private void InitializeResultCallback( Dictionary<string,Product> products,QonversionError error)
	{
		if (error == null)
		{
			Debug.Log("Product Count: " + products.Count);

			foreach (var key in products.Keys) {
				Debug.Log("Product ID: " + products[key].QonversionId);
			}

			OnInitSucess.Invoke();

		}
		else
		{
			// Handle the error  
			Debug.Log("Qonversation init products Error" + error.ToString());
			OnInitFail.Invoke();

		}
	}
	

	// automatically called after one product is bought
	// this is an example of product bought callback
	private void ProductBoughtCallback(IAPOperationStatus status, string message, StoreProduct
	product)
	{
		if (status == IAPOperationStatus.Success)
		{

			//Debug.Log("SuccessFull Purchase: " + product.GetStoreID());
			if (product.GetStoreID() == regularID) {
				OnRegularBuy.Invoke(regularID);
			}
			OnPurchase.Invoke();

			if (_check)
			{  
				OnPurchaseChecked.Invoke();
			}
			else {
				OnPurchaseUnchecked.Invoke();
			}


		}
		else
		{
			//an error occurred in the buy process, log the message for more details
			Debug.Log("Buy product failed: " + message);
		}
	}

	
	private void ProductBoughtCallback(Dictionary<string,Permission> permissions, QonversionError error)
	{
		if (error == null)
		{

			Debug.Log("SuccessFull Purchase: " + currentBuingProductID);
			if (currentBuingProductID== regularID)
			{
				OnRegularBuy.Invoke(regularID);
			}
			OnPurchase.Invoke();

			if (_check)
			{
				OnPurchaseChecked.Invoke();
			}
			else
			{
				OnPurchaseUnchecked.Invoke();
			}

			Qonversion.SyncPurchases();
		}
		else
		{
			// Handle the error  
			Debug.Log(" Qonversation buy Error" + error.ToString());
		}
	}
	
	public bool IsInit() {
		return GleyEasyIAP.IAPManager.Instance.IsInitialized();
	}


	public enum IAPManagerType
	{
		 Qonversion,
		 Regular
	}
}
