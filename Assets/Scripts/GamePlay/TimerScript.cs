﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TimerScript : MonoBehaviour {

    public float timeLeft = 10.0f;
    public Text text;
    public bool clock = true;
    private float mins;
    private float secs;

    public bool LevelCompleteOrFailed = false;

    void Start()
    {
        timeLeft = GamePlayHandler.instance.gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Time;
    }



    void Update()
    {
        if (!LevelCompleteOrFailed)
        {
            if (timeLeft > 0 && clock == false)
            {
                clock = true;
                StartCoroutine(Wait());
            }
        }
       
      
       

    }

    IEnumerator Wait()
    {
        timeLeft -= 1;
        UpdateTimer();
        yield return new WaitForSeconds(1);
        if (timeLeft == 0)
        {
            UImanager.instance.TimesUp.Invoke();
            Debug.Log("Level Failed///////////////");
        }
        else
        {
            clock = false;
        }
       
        
      
    }

    void UpdateTimer()
    {
        int min = Mathf.FloorToInt(timeLeft / 60);
        int sec = Mathf.FloorToInt(timeLeft % 60);
        text.GetComponent<UnityEngine.UI.Text>().text = min.ToString("00") + ":" + sec.ToString("00");
    }
}
