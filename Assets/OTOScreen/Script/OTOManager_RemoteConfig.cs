﻿using Firebase;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Firebase.Extensions;
using System;
using System.Linq;
using UnityEngine.Networking;
using UnityEngine.Events;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace OTO
{
    public class OTOManager_RemoteConfig : MonoBehaviour
    {

        [HideInInspector] public OTOSettings _oto_settings;
        [HideInInspector] public OTOScreen otoScreen;
         OTOScreenVisual screenVisual;
        [HideInInspector] public UnityEvent OnDataLoaded;
        [HideInInspector] public UnityEvent OnDataLoadFailed;
        [HideInInspector] public OTOEvent OnDataSend = new OTOEvent();
        Firebase.FirebaseApp app = null;
        Firebase.DependencyStatus dependencyStatus = Firebase.DependencyStatus.UnavailableOther;
        bool isFirebaseInitialized = false;

        private void Awake()
        {
            Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
                var dependencyStatus = task.Result;
                if (dependencyStatus == Firebase.DependencyStatus.Available)
                {
                    // Create and hold a reference to your FirebaseApp,
                    // where app is a Firebase.FirebaseApp property of your application class.
                    app = Firebase.FirebaseApp.DefaultInstance;

                    // Set a flag here to indicate whether Firebase is ready to use by your app.
                    EnsureInitialization();
                }
                else
                {
                    UnityEngine.Debug.LogError(System.String.Format(
                      "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
                    // Firebase Unity SDK is not safe to use here.
                }
            });
        }
        // Start is called before the first frame update
        void Start()
        {
            screenVisual = GetComponent<OTOScreenVisual>();
        }



        void SetSettings(Firebase.RemoteConfig.FirebaseRemoteConfig config)
        {

            _oto_settings = new OTOSettings(config);
            GameProps.ottoOffer = _oto_settings.offer_type;
            if (_oto_settings.hide_oto)
            {
                screenVisual.NextScene();
            }

        }
        void SetOtoScreen(Firebase.RemoteConfig.FirebaseRemoteConfig config)
        {
            otoScreen = new OTOScreen(config);
            oto_inapIds.checked_iap_product_id = otoScreen.checked_iap_product_id;
            oto_inapIds.unchecked_iap_product_id = otoScreen.unchecked_iap_product_id;
            OnDataSend.Invoke(otoScreen);
            StartCoroutine(DownloadImage(otoScreen.promotion_imageLink));
        }
        IEnumerator DownloadImage(string url)
        {
            UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
                OnDataLoadFailed.Invoke();
            }
            else
            {
                Texture2D myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                screenVisual.image.sprite = Sprite.Create(myTexture, new Rect(0.0f, 0.0f, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f));
                OnDataLoaded.Invoke();
            }


        }
        void GetAllData()
        {
           Firebase.RemoteConfig.FirebaseRemoteConfig config = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance;


          
             SetAppSettings(config);
             SetSettings(config);
             SetOtoScreen(config);

            Debug.Log("Read all data from  FireBase.");
            
        }
        void EnsureInitialization()
        {
              Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.EnsureInitializedAsync().ContinueWithOnMainThread(task=> {
                
                FetchData();
            });
        }
        void FetchData()
        { 
            //Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.ConfigSettings;
            Dictionary<string, object> defaults = new Dictionary<string, object>();
            Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults)
              .ContinueWithOnMainThread(task => {
            // [END set_defaults]
            Debug.Log("RemoteConfig configured and ready!");
                  isFirebaseInitialized = true;
                  FetchDataAsync();
              });

        }

        void SetAppSettings(Firebase.RemoteConfig.FirebaseRemoteConfig config)
        {
            app_ads_flag.is_yodo_enabled = config.GetValue("is_Yodo_Enabled").BooleanValue;
            app_ads_flag.is_admob_enabled = config.GetValue("is_AdMob_Enabled").BooleanValue;
            app_ads_flag.is_fb_adnetwork_enabled = config.GetValue("is_FB_AUD_enabled").BooleanValue;
            app_ads_flag.is_unity_enabled = config.GetValue("is_Unity_enabled").BooleanValue;
            app_ads_flag.is_ironsource_enabled = config.GetValue("is_IronSource_Enabled").BooleanValue;
            app_ads_flag.show_incomplete_reward_popup = config.GetValue("show_incomplete_reward_popup").BooleanValue;
            app_ads_flag.Hide_Ads = config.GetValue("Hide_Ads").BooleanValue;
            app_ads_flag.number_of_clicks_for_instertitial = (int)config.GetValue("number_of_clicks_for_instertitial").LongValue;

            Debug.Log("app_ads_flag  Initialized");
        }

        public void DisplayAllKeys()
        {
            Debug.Log("Current Keys:");
            System.Collections.Generic.IEnumerable<string> keys =
                Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.Keys;
            foreach (string key in keys)
            {
                Debug.Log("    " + key);
            }
            Debug.Log("GetKeysByPrefix(\"config_test_s\"):");
            keys = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.GetKeysByPrefix("config_test_s");
            foreach (string key in keys)
            {
                Debug.Log("    " + key);
            }
        }
        // [START fetch_async]
        // Start a fetch request.
        // FetchAsync only fetches new data if the current data is older than the provided
        // timespan.  Otherwise it assumes the data is "recent enough", and does nothing.
        // By default the timespan is 12 hours, and for production apps, this is a good
        // number. For this example though, it's set to a timespan of zero, so that
        // changes in the console will always show up immediately.
        public Task FetchDataAsync()
        {
            Debug.Log("Fetching data...");
            System.Threading.Tasks.Task fetchTask =
            Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.FetchAsync(
                TimeSpan.Zero);

            return fetchTask.ContinueWithOnMainThread(FetchComplete);
        }
        //[END fetch_async]

        void FetchComplete(Task fetchTask)
        {
            if (fetchTask.IsCanceled)
            {
                Debug.Log("Fetch canceled.");
            }
            else if (fetchTask.IsFaulted)
            {
                Debug.Log("Fetch encountered an error.");
            }
            else if (fetchTask.IsCompleted)
            {
                Debug.Log("Fetch completed successfully!");
               
               
            }

            var info = Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.Info;
            switch (info.LastFetchStatus)
            {
                case Firebase.RemoteConfig.LastFetchStatus.Success:
                    Firebase.RemoteConfig.FirebaseRemoteConfig.DefaultInstance.ActivateAsync()
                    .ContinueWithOnMainThread(task => {
                        Debug.Log(String.Format("Remote data loaded and ready (last fetch time {0}).",
                                   info.FetchTime));
                        GetAllData();

                    });

                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Failure:
                    switch (info.LastFetchFailureReason)
                    {
                        case Firebase.RemoteConfig.FetchFailureReason.Error:
                            Debug.Log("Fetch failed for unknown reason");
                            break;
                        case Firebase.RemoteConfig.FetchFailureReason.Throttled:
                            Debug.Log("Fetch throttled until " + info.ThrottledEndTime);
                            break;
                    }
                    break;
                case Firebase.RemoteConfig.LastFetchStatus.Pending:
                    Debug.Log("Latest Fetch call still pending.");
                    break;
            }
        }
    }
}
