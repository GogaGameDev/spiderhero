﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class MainMenuHandler : MonoBehaviour
{
    #region
    public static MainMenuHandler Instace;
    [Header("Hand Img")]
    public GameObject Play;
    public GameObject Logo;
    [Header(" Setting Buttons")]
    public Button SettingBtn;
    public Button MusicBtn;
    public Button SoundBtn;
    bool SettingOpen = false;
    [Header("Rate US Links")]
    public string RateUsAndriod = "";
    public string RateUsIOS = "";
    [Header("PrivacyPolicy")]
    public string PrivacyPolicy = "";
    [Header("MoreGames")]
    public string  MoreGameAndriod = "";
    public string  MoreGameIOS = "";
    [Header("setting Btns")]
    public GameObject Sound;
    public GameObject Music;
    public bool SettingOn = false;
    [Header("Music & Sound")]
    public Button EffectsButton;
    public Button MusicButton;
    public Sprite EffectButtonEnable;
    public Sprite EffectButtonDisable;
    public Sprite MusicButtonEnable;
    public Sprite MusicButtonDisable;
    [Header("Scriptable")]
    public GameData Gdata;
    bool FirstTouch = true;
    [Header("Panels")]
    public GameObject InfoPanel;
    public GameObject SettingPanel;
    public GameObject ModePanel;
    [Header("Removeads")]
    public Button RemoveAds;
    public GameObject RemoveAdstext;
    public Text CoinsText;
    public GameObject ExitManager;
    #endregion
    private void Awake()
    {
        if(Instace == null)
        {
            Instace = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
        SoundManager.Instance.PlayBackgroundMusic(AudioClipsSource.Instance.MainMenuClip);
        Play.transform.DOScale(1.05f, 0.5f).SetLoops(-1, LoopType.Yoyo);
        Logo.transform.DOScale(1.1f, 0.8f).SetLoops(-1, LoopType.Yoyo);
        if (!Gdata.RemoveAds)
        {
            //RemoveAds.transform.DOScale(0.95f, 0.2f).SetLoops(-1, LoopType.Yoyo);
           // if (MonetizationManager.instance.interstitial==null||!MonetizationManager.instance.interstitial.IsLoaded())
          //  {
            //    MonetizationManager.instance.LoadInterstitial();
           // }
            Debug.Log("Ready to Show Unity Banner");
            //MonetizationManager.instance.ShowUnityBanner();
            //MonetizationManager.instance.showBannerAd();

        }
      
        CoinsText.text = Gdata.Coins.ToString();

        int showInter = PlayerPrefs.GetInt("showInter", 0);
        if (showInter == 1 && !Gdata.RemoveAds)
        {
            MonetizationManager.instance.ShowInterstitialMediation();
        }
    }
    public void OnPlayBtnClick()
    {
          SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
          GameManager.Instance.ChangeScene("LevelSelection");
        MonetizationManager.instance.ShowAdmobInterstitialMediation();
    }
    public void OpenInfo()
    {
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        InfoPanel.SetActive(true);
        InfoPanel.GetComponent<InfoHandler>().OpenPanel();
    }

    public void OnGameModeBtnClick()
    {
        ModePanel.SetActive(true);
        ModePanel.GetComponent<ModeSelectionHandler>().OpenPanel();
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);

    }
    public void SelectMode(int ModeNo)
    {
        GameManager.Instance.ModSelected = ModeNo;
        switch (GameManager.Instance.ModSelected)
        {
            case 0:
                GameManager.Instance.ChangeScene("LevelSelection");
                break;
            case 1:
                GameManager.Instance.ChangeScene("BikeSelection");
                break;

        }
    }
    public void OpenSetting()
    {
        if (!SettingOn) {
            SettingOn = true;
            ExitManager.SetActive(false);
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        SettingPanel.SetActive(true);
        SettingPanel.GetComponent<SettingHandler>().OpenSetting();
        }
        else
        {
            SettingOn = false;
            ExitManager.SetActive(true);
            SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
            SettingPanel.GetComponent<SettingHandler>().CloseSetting();
        }
    }

    private bool musicStatus = true;
    private bool effectStatus = true;

    public void EffectsEnable()
    {
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.CloseButtonClip);
        effectStatus = !effectStatus;

        if (EffectButtonEnable == null && EffectButtonDisable == null)
            return;

        if (effectStatus)
        {
            EffectsButton.gameObject.GetComponent<Image>().overrideSprite = EffectButtonEnable;
        }
        else
        {
            EffectsButton.gameObject.GetComponent<Image>().overrideSprite = EffectButtonDisable;
        }
        SoundManager.Instance.EffectsEnabled = effectStatus;
        Debug.Log("Effects Status" + effectStatus);
    }

    public void MusicEnable()
    {
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.CloseButtonClip);
        musicStatus = !musicStatus;


        if (MusicButtonEnable == null && MusicButtonDisable == null)
            return;

        if (musicStatus)
        {
            MusicButton.gameObject.GetComponent<Image>().overrideSprite = MusicButtonEnable;
        }
        else
        {
            MusicButton.gameObject.GetComponent<Image>().overrideSprite = MusicButtonDisable;
        }

        SoundManager.Instance.MusicEnabled = musicStatus;
        Debug.Log("Music Status" + musicStatus);

    }
    public void RateUsClick()
    {
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
#if UNITY_ANDROID
        Application.OpenURL(RateUsAndriod);
#elif UNITY_IOS
        Application.OpenURL(RateUsIOS);
#endif
    }
    public void PrivacyPolicyBtnClick()
    {
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        Application.OpenURL(PrivacyPolicy);
    }
    public void TermsOfUseBtnClick()
    {
        string terms = "https://www.digitalmarketerhk.com/apps-terms-of-use";
        Application.OpenURL(terms);
    }
    public void MoreGame()
    {
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
#if UNITY_ANDROID
        Application.OpenURL(MoreGameAndriod);
#elif UNITY_IOS
        Application.OpenURL(MoreGameIOS);
#endif
    }
    public void ExitBtnClick()
    {
        Application.Quit();
    }
}
