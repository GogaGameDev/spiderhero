using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyBtn : MonoBehaviour {

    public enum ItemType
    {
        Gold100,
        Gold5000,
        Gold10000,
        NoAds   
    }
    public ItemType itemType;
    public Text PriceText;
    public string defaultText;


    private void Start()
    {
       // defaultText = PriceText.text;
        //StartCoroutine(LoadPriceRoutine());
    }

    public void OnClick()
    {
        switch (itemType)
        {
            case ItemType.Gold100:
                IAPManager.instance.BuyGold100();
                break;
            //case ItemType.Gold5000:
            //    IAPManager.instance.BuyGold5000();
            //    break;
            //case ItemType.Gold10000:
            //    IAPManager.instance.BuyGold10000();
            //    break;
            case ItemType.NoAds:
                IAPManager.instance.BuyNoAds();
                break;
            
        }

    }
    //private IEnumerator LoadPriceRoutine()
    //{
    //    while (!IAPManager.instance.IsInitialized())
    //    {
    //        yield return null;
    //    }
    //    //string loaderPrice = "";

    //    switch (itemType)
    //    {
    //        case ItemType.Gold2000:
    //            IAPManager.instance.BuyGold2000();
    //            break;
    //        case ItemType.Gold5000:
    //            IAPManager.instance.BuyGold5000();
    //            break;
    //        case ItemType.Gold10000:
    //            IAPManager.instance.BuyGold10000();
    //            break;
    //        case ItemType.NoAds:
    //            IAPManager.instance.BuyNoAds();
    //            break;

    //    }
    //    // PriceText.text = defaultText + " " + loaderPrice;
    //}
}
