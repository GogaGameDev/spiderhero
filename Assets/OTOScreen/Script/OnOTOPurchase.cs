﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace OTO
{
    public class OnOTOPurchase : MonoBehaviour
    {
#if UNITY_EDITOR
        public bool test;
#endif
        public GameData data;
        private void Awake()
        {
#if UNITY_EDITOR
            if (test)
            {
                PlayerPrefs.DeleteAll();
                data.RemoveAds = false;
                data.UnLockedRickshaw3 = false;
                data.UnLockedRickshaw2 = false;
                data.Coins = 0;
            }
#endif
            IAP iap = GetComponent<IAP>();
            iap.OnPurchaseChecked.AddListener(OnChecked);
            iap.OnPurchaseUnchecked.AddListener(OnUncheacked);
        }
        private void Start()
        {
            if (!data.RemoveAds)
            {
               // MonetizationManager.instance.LoadInterstitial();
            }
        }
        void OnChecked()
        {
            FireBaseAnalitycs.instance.OnOTOBuyCheckMark_Send(GameProps.ottoOffer);
            Debug.Log("OnChecked Purchase");
            PlayerPrefs.SetInt("noads", 1);
            PlayerPrefs.SetInt("Level", 14);
            data.RemoveAds = true;
            data.Coins += 90000;
            UnlockAds();
            data.UnLockedRickshaw2 = true;
            data.UnLockedRickshaw3 = true;
        }
        void OnUncheacked()
        {
            FireBaseAnalitycs.instance.OnOTOBuyWithoutCheckMark_Send(GameProps.ottoOffer);
            Debug.Log("OnUncheacked Purchase");
            PlayerPrefs.SetInt("Level", 14);
            data.Coins += 90000;
          
            if (app_ads_flag.all_oto_iap_unlock_ads)
            {
                UnlockAds();
            }
        }

        void UnlockAds()
        {
            data.RemoveAds = true;
        }
    }

}
