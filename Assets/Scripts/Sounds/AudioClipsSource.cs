using UnityEngine;


public class AudioClipsSource : MonoBehaviour
{

    [Header("Music Clips")]
    public AudioClip MainMenuClip;
    public AudioClip LevelSelectionClip;
    public AudioClip GamePlayClip;

    public AudioClip PlayButtonClick;
    public AudioClip CloseButtonClip;
    public AudioClip GenericButtonClip;

    public AudioClip LevelFailedClip;
    public AudioClip LevelSuccessClip;
    public AudioClip CollectItem;
    public AudioClip Chase;
    public AudioClip Chips;
    public AudioClip Alert;
   


    public static AudioClipsSource Instance;

    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            DontDestroyOnLoad(this.gameObject);
        }
    }







}
