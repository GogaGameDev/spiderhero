﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class GamePlayHandler : MonoBehaviour
{
    #region Attributes
    [Header("Players")]
    public GameObject[] Player;
    public GameObject[] PlayerDriver;
    public GameObject Camera;
    public Canvas PlayerCanvas;
    [Header("PartolFillingPoints")]
    public GameObject[] PartolFillingPoint;
    [Header("Drop Off Location Object")]
    public GameObject DropOffLocation;
    public int currentDropPoint;
    [Header("Pick Up Location Object")]
    public GameObject PickUpLocation;
    public int currentPickPoint;
    public int NumberOffPassengerDroping;
    [Header("Game Modes")]
    public GameModes[] gameModes;
    public static GamePlayHandler instance;
    public int RandNo;
    public int RandNoDrop;
    float CineLen;
    public int TodaysEarning;
    public int coins;

    #endregion
    private void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(PlayCineMatic());
        ActivePlayer();
        ActivePickUpLocation();
        //if (!GameManager.Instance.Gdata.RemoveAds)
        //{
        //    MonetizationManager.instance.HideBannerAd();
        //}
    }
    private void ActivePlayer()
    {
        switch (GameManager.Instance.ModSelected)
        {
            case 0:
                for (int i = 0; i < Player.Length; i++)
                {
                    if (i == GameManager.Instance.BikeSelected)
                    {
                        Player[i].transform.position = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].PlayerPos.position;
                        Player[i].transform.rotation = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].PlayerPos.rotation;
                        Player[i].SetActive(true);
                    }
                }
                break;
            case 1:
                for (int i = 0; i < Player.Length; i++)
                {
                    if (i == GameManager.Instance.BikeSelected)
                    {
                        Player[i].transform.position = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].PlayerPos.position;
                        Player[i].transform.rotation = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].PlayerPos.rotation;
                        Player[i].SetActive(true);
                    }
                }
                break;
        }

    }
    public void ActivePartolFIllingPoints()
    {
        if (PartolFillingPoint.Length > 0)
        {
            for (int i = 0; i < PartolFillingPoint.Length; i++)
            {
                PartolFillingPoint[i].SetActive(true);
            }
        }
    }
    public void DisactivePartolFIllingPoints()
    {
        if (PartolFillingPoint.Length > 0)
        {
            for (int i = 0; i < PartolFillingPoint.Length; i++)
            {
                PartolFillingPoint[i].SetActive(false);
            }
        }
    }
    public void ActiveDropOffLocation()
    {
        switch (GameManager.Instance.ModSelected)
        {
            case 0:
                for (int i = 0; i < gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].DropOffLocation.Length; i++)
                {
                    if (i == currentPickPoint)
                    {
                        DropOffLocation.transform.position = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].DropOffLocation[i].position;
                        DropOffLocation.transform.rotation = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].DropOffLocation[i].rotation;
                    }
                }
                DropOffLocation.SetActive(true);
                break;
            case 1:
                RandNoDrop = Random.Range(0, 3);
                for (int i = 0; i < gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].DropOffLocation.Length; i++)
                {
                    if (i == RandNoDrop)
                    {
                        DropOffLocation.transform.position = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].DropOffLocation[i].position;
                        DropOffLocation.transform.rotation = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].DropOffLocation[i].rotation;
                    }
                }
                Debug.Log("In 2nd mode");
                DropOffLocation.SetActive(true);
                break;
        }


    }
    public void ActivePickUpLocation()
    {
        switch (GameManager.Instance.ModSelected)
        {
            case 0:
                if (gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].PickPos.Length > 0)
                {
                    for (int i = 0; i < gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].PickPos.Length; i++)
                    {
                        if (i == currentPickPoint)
                        {
                            PickUpLocation.transform.position = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].PickPos[i].position;
                            PickUpLocation.transform.rotation = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].PickPos[i].rotation;
                            gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].Passangers[i].SetActive(true);
                        }
                    }
                    Debug.Log("Activating Passanger");
                    PickUpLocation.SetActive(true);
                }
                break;
            case 1:
                RandNo = Random.Range(0, 3);
                for (int i = 0; i < gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].PickPos.Length; i++)
                {
                    if (i == RandNo)
                    {
                        PickUpLocation.transform.position = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].PickPos[i].position;
                        PickUpLocation.transform.rotation = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].PickPos[i].rotation;
                        gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].Passangers[i].transform.position = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].PassangersPos[i].position;
                        gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].Passangers[i].transform.rotation = gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].PassangersPos[i].rotation;
                        gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].Passangers[i].SetActive(true);
                    }
                }
                PickUpLocation.SetActive(true);
                break;
        }

    }
    public void PlayGame()
    {
        PlayerCanvas.enabled = true;
        
    }
    IEnumerator PlayCineMatic()
    {
        switch (GameManager.Instance.ModSelected)
        {
            case 0:
                if (gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].CinematicCam != null)
                {
                    UImanager.instance.ObjectiveImg.SetActive(true);
                    gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].CinematicCam.SetActive(true);
                }
                switch (GameManager.Instance.LevelSelected)
                {
                    case 0:
                        CineLen = 11f;
                        break;

                }
                yield return new WaitForSeconds(CineLen);
                Camera.SetActive(true);
                PlayGame();
                //PlayerDriver[GameManager.Instance.BikeSelected].SetActive(true);
                UImanager.instance.ObjectiveImg.SetActive(false);

                if (gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].CinematicCam != null)
                {
                    UImanager.instance.ObjectiveImg.SetActive(false);
                    gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.LevelSelected].CinematicCam.SetActive(false);
                }
                //if (GameManager.Instance.LevelSelected > 0)
                //{
                //}
                //else
                //{
                //    PlayerCanvas.enabled = true;

                //}
                break;
            case 1:
                if (gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].CinematicCam != null)
                {
                    gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].CinematicCam.SetActive(true);
                }
                switch (GameManager.Instance.FreeRoamSelected)
                {
                    case 0:
                        CineLen = 9f;
                        break;

                }
                yield return new WaitForSeconds(CineLen);
                Camera.SetActive(true);
                PlayGame();
                PlayerDriver[GameManager.Instance.BikeSelected].SetActive(true);
                if (gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].CinematicCam != null)
                {
                    gameModes[GameManager.Instance.ModSelected].Levels[GameManager.Instance.FreeRoamSelected].CinematicCam.SetActive(false);
                }
                break;
        }

    }
}
#region Classes
[System.Serializable]
public class GameModes
{
    public GameLevels[] Levels;
}
[System.Serializable]
public class GameLevels
{
    [Header("Player Postion In Every Level")]
    public Transform PlayerPos;
    [Header("Drop off Location")]
    public Transform[] DropOffLocation;
    [Header("Number of Passengers in every Level")]
    public GameObject[] Passangers;
    public Transform[] PassangersPos;
    [Header("Number PickPos")]
    public Transform[] PickPos;
    public GameObject CinematicCam;
    public float Time;

}
#endregion




