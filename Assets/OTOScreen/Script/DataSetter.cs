﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OTO
{
    public class DataSetter : MonoBehaviour
    {

        void Start()
        {
            GameObject dataManagerObject = GameObject.Find("OTOManager");
            dataManagerObject.GetComponent<OTOManager_RemoteConfig>().OnDataSend.AddListener(DateSet);
            // PlayerPrefs.SetInt("CharUnlocked", 0);
        }

        // Update is called once per frame
        void Update()
        {

        }

        void DateSet(OTOScreen oto)
        {
            GameProps.regularInapID = oto.regular_iap_product_id;
        }

    }
}
