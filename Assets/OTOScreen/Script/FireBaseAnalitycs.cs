﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
namespace OTO
{
    public class FireBaseAnalitycs : Singleton<FireBaseAnalitycs>
    {

        private void Awake()
        {
            DontDestroyOnLoad(this.gameObject);
        }

        public void OnOTOShownSend(string OTOType)
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent(OTOType + "_Shown");
        }
        public void OnOTOBuyCheckMark_Send(string OTOType)
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Unlock_Button_tap_" + OTOType + "_OB");
        }
        public void OnOTOBuyWithoutCheckMark_Send(string OTOType)
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Unlock_Button_tap_" + OTOType + "_NO_OB");
        }

        public void OnSkipOTOEvent_Send(string OTOType)
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("NoThanks" + OTOType + "_Tap");
        }


        public void OnBannerAdShown_Send()
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Banner_Ad_Shown");


        }

        public void OnIntersistialAdShown_Send()
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Interstitial_Ad_Shown");

        }
        public void OnRewardAdShown_Send()
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Reward_Ad_Shown");

        }

        public void OnBannerTap_Send()
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Banner_Ad_Tap");

        }
        public void OnInterstitialTap_Send()
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Banner_Ad_Tap");

        }
        public void OnRewardTap_Send()
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Banner_Ad_Tap");

        }

        public void OnRewardAdCompleteInside_Send()
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("Reward_Ad_Inside_Rewarded");


        }

    }
}
