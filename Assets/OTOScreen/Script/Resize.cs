﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OTO
{
    public class Resize : MonoBehaviour
    {
        [SerializeField] float originalHeight;
        [SerializeField] float originalPosY;
        [SerializeField] float safeAreaHeight;
        [SerializeField] float safeAreaPosY;
        [SerializeField] bool safeArea;

        private void Awake()
        {
            RectTransform rect = GetComponent<RectTransform>();

            safeArea = Screen.currentResolution.height != Screen.safeArea.height;

            if (safeArea)
            {
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, safeAreaHeight);
                rect.anchoredPosition = new Vector3(rect.localPosition.x, safeAreaPosY);
            }
            else
            {
                rect.sizeDelta = new Vector2(rect.sizeDelta.x, originalHeight);
                rect.anchoredPosition = new Vector3(rect.localPosition.x, originalPosY);
            }

        }


    }
}
