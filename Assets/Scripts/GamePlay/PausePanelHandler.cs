﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
public class PausePanelHandler : MonoBehaviour
{
    public GameObject Resume, Home, Restart, Logo,FG;

    public void OpenPausePanel()
    {
        UImanager.instance.WayPointCanvas.GetComponent<Canvas>().enabled = false;
        GamePlayHandler.instance.PlayerCanvas.enabled = false;
        FG.transform.DOLocalMoveX(0.1f, 0.3f).OnComplete(delegate
        {

        //    Logo.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBounce).OnComplete(delegate
        //{
            Home.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBounce).OnComplete(delegate
            {
                Restart.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBounce).OnComplete(delegate
                {
                    Resume.transform.DOScale(1f, 0.2f).SetEase(Ease.OutBounce).OnComplete(delegate
                    {
                        Resume.transform.DOScale(0.9f, 0.2f).SetLoops(-1, LoopType.Yoyo).SetUpdate(true);
                        MonetizationManager.instance.ShowInterstitialMediation();
                        Time.timeScale = 0;
                    });
                //});
            });
        });
        });

    }
    public void ClosePausePanel()
    {
        UImanager.instance.WayPointCanvas.GetComponent<Canvas>().enabled = true;
        Resume.transform.DOScale(0f, 0.2f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
        {
            Restart.transform.DOScale(0f, 0.2f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
            {
                Home.transform.DOScale(0f, 0.2f).SetEase(Ease.OutBounce).SetUpdate(true).OnComplete(delegate
                {

                    FG.transform.DOLocalMoveX(-1198f, 0.3f);
                    GamePlayHandler.instance.PlayerCanvas.enabled = true;
                    //GamePlayHandler.instance.Player.SetActive(true);
                    UImanager.instance.PausePanel.SetActive(false);
                    Time.timeScale = 1;
                });
            });
        });

    }
}
