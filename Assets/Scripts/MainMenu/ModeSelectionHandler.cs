﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
public class ModeSelectionHandler : MonoBehaviour
{
    public GameObject FG;
    public GameObject FreeRoam;
    public GameObject CareerMode;
    public GameObject Back;
    public Text Alert;
    public void OpenPanel()
    {
        FG.transform.DOLocalMoveX(0.1f, 0.4f).OnComplete(delegate
        {
            FreeRoam.transform.DOScale(1, 0.2f).OnStepComplete(delegate {
                CareerMode.transform.DOScale(1, 0.2f).OnStepComplete(delegate {
                    Back.transform.DOScale(1, 0.2f).OnStepComplete(delegate {


                    });

                });

            });

        });
    }
    public void ClosePanel()
    {
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        Back.transform.DOScale(0, 0.2f).OnStepComplete(delegate {
            CareerMode.transform.DOScale(0, 0.2f).OnStepComplete(delegate {
                FreeRoam.transform.DOScale(0, 0.2f).OnStepComplete(delegate {
                    FG.transform.DOLocalMoveX(-901f, 0.4f).OnComplete(delegate
                    {
                        MainMenuHandler.Instace.ModePanel.SetActive(false);
                    });

                });

            });

        });

    }
    public void NextPanel()
    {

        //MainMenuHandler.Instace.ModePanel.SetActive(false);
        switch (GameManager.Instance.ModSelected)
        {
            case 0:
                SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
                Back.transform.DOScale(0, 0.2f).OnStepComplete(delegate {
                    CareerMode.transform.DOScale(0, 0.2f).OnStepComplete(delegate {
                        FreeRoam.transform.DOScale(0, 0.2f).OnStepComplete(delegate {
                            FG.transform.DOLocalMoveX(901f, 0.4f).OnComplete(delegate
                            {

                                GameManager.Instance.ChangeScene("LevelSelection");

                            });

                        });

                    });
                });

                break;
            case 1:
                if(GameManager.Instance.Gdata.LevelCompleted > 2) {
                    SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
                    Back.transform.DOScale(0, 0.2f).OnStepComplete(delegate {
                CareerMode.transform.DOScale(0, 0.2f).OnStepComplete(delegate {
                    FreeRoam.transform.DOScale(0, 0.2f).OnStepComplete(delegate {
                        FG.transform.DOLocalMoveX(901f, 0.4f).OnComplete(delegate
                        {

                            GameManager.Instance.ChangeScene("RickshawSelection");
                        });

                    });

                });
                });
                }
                else
                {
                    MsgAlert();
                }
                break;
        }
    }
    public void MsgAlert()
    {
        SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.Alert);
        Alert.gameObject.SetActive(true);
        FreeRoam.GetComponent<Button>().interactable = false;
        CareerMode.GetComponent<Button>().interactable = false;
        Alert.text = "You must Complete 3 Levels of Career Mode to play free Roam";
        Alert.transform.DOScale(1.1f, 0.8f).OnComplete(delegate {
            Alert.transform.DOScale(1f, 0.8f).OnComplete(delegate {
                Alert.gameObject.SetActive(false);
                FreeRoam.GetComponent<Button>().interactable = true;
                CareerMode.GetComponent<Button>().interactable = true;

            });
        });
    }
}
