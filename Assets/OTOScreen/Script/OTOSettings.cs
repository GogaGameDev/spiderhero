﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace OTO
{
    public class OTOSettings
    {
       // public List<string> iap_product_ids;
        public bool no_thanks_off_button;
        public string offer_type;
        public bool rewarded_off;
        public bool show_oto_on_app_starts;
      //  public int type;
        public bool hide_oto;

        public OTOSettings(Dictionary<string, object> _params)
        {

           // iap_product_ids = new List<string>();
            //foreach (var item in (List<object>)(_params["iap-product-ids"]))
          //  {
          //      iap_product_ids.Add((string)item);
          //  }
            no_thanks_off_button = (bool)_params["no-thanks-off-button"];
            offer_type = (string)_params["offer-type"];
            rewarded_off = (bool)_params["rewarded-off"];
            hide_oto = (bool)_params["hide-otto"];
            show_oto_on_app_starts = (bool)_params["show-oto-on-app-starts"];
           // type = (int)((Int64)_params["type"]);

        }

        public OTOSettings(Firebase.RemoteConfig.FirebaseRemoteConfig config)
        {
            
         //  iap_product_ids = new List<string>();
           // foreach (var item in (List<object>)(_params["iap-product-ids"]))
          //  {
          //      iap_product_ids.Add((string)item);
          //  }
            no_thanks_off_button = config.GetValue("NoThanksBtnOFF").BooleanValue;
            offer_type = config.GetValue("Offer_Type").StringValue;
            rewarded_off = config.GetValue("RewardOFF").BooleanValue;
            hide_oto = config.GetValue("HideOTO").BooleanValue;
            show_oto_on_app_starts = config.GetValue("showOTOOnAppStart").BooleanValue;
           // type = (int)((Int64)_params["type"]);
            

        }
    }
}
