﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class LevelHandler : MonoBehaviour
{
    public GameObject fG;
    public GameObject Back;
    public GameObject[] Levels;
    public GameObject[] Locks;
    //public GameObject[] Frames;
    //public GameData GData;
    private void Start()
    {
        fG.transform.DOLocalMoveX(0.1f,0.4f).OnComplete(delegate {
            Back.transform.DOScale(1f, 0.2f);
            StartCoroutine(BtnsShow());
        
        });
        if(PlayerPrefs.GetInt("Level") > 14)
        {
            PlayerPrefs.SetInt("Level", 14);
        }
        //Debug.Log(PlayerPrefs.GetInt("UnlockLevels"));
        //PlayerPrefs.SetInt("UnlockLevels", 2);

        int LevelUnlocker = PlayerPrefs.GetInt("Level");
        for (int i = 0; i < Levels.Length; i++)
        {
            if (i > LevelUnlocker)
            {
                Levels[i].GetComponent<Button>().interactable = false;
                Locks[i].SetActive(true);
            }
            else
            {
                Levels[i].GetComponent<Button>().interactable = true;
                Locks[i].SetActive(false);
            }
        }
        //Frames[LevelUnlocker].SetActive(true);
        Levels[LevelUnlocker].transform.DOScale(0.96f, 0.4f).SetLoops(-1, LoopType.Yoyo);
    }
    IEnumerator BtnsShow()
    {
        for (int i = 0; i < Levels.Length; i++)
        {
            yield return new WaitForSeconds(0.2f);
            Levels[i].SetActive(true);
        }
    }
    public void SelectLevel(int level)
    {
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        GameManager.Instance.LevelSelected = level;
        Back.transform.DOScale(0f, 0.2f);
        fG.transform.DOLocalMoveX(1150f, 0.4f).OnComplete(delegate {
            GameManager.Instance.ChangeScene("BikeSelection");


        });
        //if(GameManager.Instance.Gdata.LevelCompleted > 0) { 
        //MonetizationManager.instance.ShowInterstitialMediation();
        //}
    }
    public void Home()
    {
        //SoundManager.Instance.PlayEffect(AudioClipsSource.Instance.GenericButtonClip);
        Back.transform.DOScale(0f, 0.2f);
        fG.transform.DOLocalMoveX(-1150f, 0.4f).OnComplete(delegate
        {

            GameManager.Instance.ChangeScene("MainMenu");
        });
    }
}
